#!/usr/bin/python

from twisted.internet import reactor
from twisted.enterprise import adbapi
from bnsession import BnSession
from chatlogging import ChatLoggingService
from trivia import TriviaService
from quote import QuoteService
from horoscope import HoroscopeService
from urbandictionary import UrbanDictionaryService
from definition import DefinitionService
from rps import RPSService
from magic8ball import Magic8BallService
from bnutil import *
from potbotextra import PotbotCommand
from potbotconfig import PotbotConfig
from Queue import Queue # TODO: this should be a deque
import logging
import time
import datetime
import random
import subprocess
import sys

class Potbot(object):
    def __init__(self, config):
        self.config = config
        self.init_log("potbot.log")
        self.commands = []
        self.init_commands()
        self.connect_db()
        self.services = {}
        self.init_services()
        self.log("Starting potbot")
        self.bn_session = BnSession(
                self, self.config.username, self.config.password,
                self.config.product, self.config.cdkey, self.config.owner,
                self.config.home_channel, self.config.bnls_address,
                self.config.bnls_port, self.config.bncs_address,
                self.config.bncs_port)
        self.message_queue = Queue()
        self.message_send_window = 4.0
        self.time_send_window = 2.0
        self.min_next_message_delay = 3 # TODO: this should be lowered, but
                                        # we need to start taking into account
                                        # the length of the previously sent
                                        # messages and the message being sent
        self.message_send_factor_weight = 1.5
        self.time_send_factor_weight = 1 - self.message_send_factor_weight
        self.message_delay_generator = self.get_next_message_delay()
        self.logon_time = None

    def init_log(self, log_name):
        formatter = logging.Formatter(
                "%(asctime)s %(message)s",
                "(%m-%d-%y %H:%M:%S)")
        handler = logging.FileHandler(log_name, mode="w")
        handler.setFormatter(formatter)
        handler.setLevel(logging.DEBUG)
        self.logger = logging.getLogger(__name__)
        self.logger.addHandler(handler)
        self.logger.setLevel(logging.DEBUG)

    def init_services(self):
        chat_log_service = ChatLoggingService(self)
        trivia_service = TriviaService(self)
        quote_service = QuoteService(self)
        horoscope_service = HoroscopeService(self)
        urban_dictionary_service = UrbanDictionaryService(self)
        definition_service = DefinitionService(self)
        rps_service = RPSService(self)
        magic8ball_service = Magic8BallService(self)
        self.add_service("logging", chat_log_service)
        self.add_service("trivia", trivia_service)
        self.add_service("quotes", quote_service)
        self.add_service("horoscopes", horoscope_service)
        self.add_service("urbandictionary", urban_dictionary_service)
        self.add_service("definition", definition_service)
        self.add_service("rps", rps_service)
        self.add_service("8ball", magic8ball_service)

    def add_service(self, service_name, service):
        self.services[service_name] = service

    def init_commands(self):
        self.add_command("quit", 100, self.command_quit)
        self.add_command("say", 90, self.command_say)
        self.add_command("join", 90, self.command_join)
        self.add_command("add", 100, self.command_add)
        self.add_command("set", 100, self.command_add)
        self.add_command("whoami", 10, self.command_whoami)
        self.add_command("whois", 10, self.command_whois)
        self.add_command("del", 10, self.command_del)
        self.add_command("roll", 30, self.command_roll)
        self.add_command("flip", 30, self.command_flip)
        self.add_command("reload", 100, self.command_reload)
        self.add_command("uptime", 30, self.command_uptime)

    def add_command(self, name, access, callback):
        self.commands.append(PotbotCommand(name, access, callback))

    def connect_db(self):
        try:
            #self.dbpool = adbapi.ConnectionPool("psycopg2", "host={} dbname=potbot".format(self.config.db_address))
            self.dbpool = adbapi.ConnectionPool("psycopg2", "dbname=potbot")
        except Exception as e:
            self.log("Could not connect to main db.  Commands will be disabled.")
            self.dbpool = None

    def logon(self):
        self.log("Attempting battle.net logon as user %s" % self.config.username)
        self.bn_session.logon()

    def logged_on(self, username):
        self.logon_time = datetime.datetime.today()
        for service_name,service in self.services.iteritems():
            service.logged_on(username)
            #reactor.callLater(5, service.start)
        self.work_message_queue()

    def get_channel_users(self):
        return self.bn_session.channel.users

    def got_showuser_event(self, username, flags, text, ping):
        pass

    def got_join_event(self, username, flags, text, ping):
        user = BnUser(username, flags, ping)
        for service_name,service in self.services.iteritems():
            service.channel_join_received(user)

    def got_leave_event(self, username, flags, text, ping):
        user = BnUser(username, flags, ping)
        for service_name,service in self.services.iteritems():
            service.channel_leave_received(user)

    def got_whisper_event(self, username, flags, text, ping):
        user = BnUser(username, flags, ping)
        message = BnMessage(user, text)
        self.check_for_command(message)
        for service_name,service in self.services.iteritems():
            service.whisper_message_received(message)

    def got_talk_event(self, username, flags, text, ping):
        user = BnUser(username, flags, ping)
        message = BnMessage(user, text)
        self.check_for_command(message)
        for service_name,service in self.services.iteritems():
            service.chat_message_received(message)

    def got_broadcast_event(self, username, flags, text, ping):
        pass
    
    def got_channel_event(self, username, flags, text, ping):
        channel = text
        for service_name,service in self.services.iteritems():
            service.joined_new_channel(channel)

    def got_userflags_event(self, username, flags, text, ping):
        pass
    
    def got_channeldoesnotexist_event(self, username, flags, text, ping):
        pass

    def got_channelrestricted_event(self, username, flags, text, ping):
        pass
    
    def got_info_event(self, username, flags, text, ping):
        for service_name,service in self.services.iteritems():
            service.info_received(text)

    def got_error_event(self, username, flags, text, ping):
        pass

    def got_emote_event(self, username, flags, text, ping):
        user = BnUser(username, flags, ping)
        message = BnMessage(user, text)
        for service_name,service in self.services.iteritems():
            service.emote_message_received(message)

    def send_chat_message(self, message_text):
        self.log("Sending chat message: {}".format(message_text))
        prefix = ""
        if is_emote(message_text):
            prefix = "/me "
            message_text = " ".join(message_text.split(" ")[1:])
        remainder = message_text
        while len(remainder) > 0:
            chunk,remainder = self.divide_message(remainder)
            self.message_queue.put(prefix + chunk)
            if len(remainder) > 0:
                remainder = "[more] " + remainder
        #self.work_message_queue()

    def divide_message(self, message):
        if len(message) <= 224: # TODO: this should be a constant
            return message,""
        split_point = message[0:224].rfind(" ")
        if split_point == -1:
            chunk = message[0:224]
            remainder = message[224:]
        else:
            chunk = message[0:split_point]
            remainder = message[split_point+1:]
        return chunk, remainder

    def work_message_queue(self):
        if self.message_queue.empty():
            reactor.callLater(.5, self.work_message_queue) #TODO: make .5 a constant
            return
        to_send = self.message_queue.get()
        self.bn_session.send_chat_message(to_send)
        for service_name,service in self.services.iteritems():
            if is_emote(to_send):
                service.emote_message_sent(to_send)
            else:
                service.chat_message_sent(to_send)
        sent_msg_len = len(to_send)
        next_queue_check = self.message_delay_generator.next()
        next_queue_check += (sent_msg_len / 224) * 4
        print "next queue check: " + str(next_queue_check)
        reactor.callLater(next_queue_check, self.work_message_queue)

    def get_next_message_delay(self):
        messages_in_window = [time.time()]
        yield self.min_next_message_delay
        while True:
            now = time.time()
            messages_in_window.append(now)
            for message_time in messages_in_window:
                if now - message_time > self.time_send_window:
                    messages_in_window.remove(message_time)
            num_messages_in_window = len(messages_in_window)
            message_factor = num_messages_in_window/self.message_send_window
            if len(messages_in_window) > 1:
                time_factor = (now - messages_in_window[-2]) / \
                              self.time_send_window
            else:
                time_factor = 0
            print "time_factor={:.3f}, message_factor={:.3f}" \
                  .format(time_factor, message_factor)
            computed_delay = (self.time_send_factor_weight*time_factor +
                    self.message_send_factor_weight*message_factor)
            yield max(computed_delay, self.min_next_message_delay)

    def check_for_command(self, message):
        user = message.user
        command_text = self.extract_command_text(message)
        if command_text == "":
            return False
        command_name, command_args = self.parse_command_text(command_text)
        command_name_lower_case = command_name.lower()
        for command in self.commands:
            if command_name_lower_case == command.name:
                #if command.user_authorized(user):
                #    command.execute(command_args)
                self.execute_if_authorized(user, command, command_args)
                return True
        return False

    def execute_if_authorized(self, user, command, args):
        if not self.external_commands_enabled():
            self.log("Got an attempted %s command, but external commands are disabled" % command.name)
            return
        username = user.username
        access_entries_deferred = self.dbpool.runQuery("select access,flags from bot_user where username ilike %s", (username,))
        access_entries_deferred.addCallback(self.received_access_entries, username, command, args)

    def external_commands_enabled(self):
        return self.dbpool is not None

    def received_access_entries(self, access_entries, username, command, args):
        if len(access_entries) < 1:
            self.log("Could not find user %s in user access db" % username)
            return
        rank,flags = access_entries[0]
        if rank >= command.required_access:
            command.execute(username, args)
        elif False: #TODO: check flags
            command.execute(username, args)
        else:
            self.log("User %s has insufficient access for command %s" % (username, command.name))

    def command_add(self, caller, args):
        if len(args) < 2:
            return
        arg_username = args[0]
        arg_access = args[1]
        arg_flags = args[2] if len(args) > 2 else ""
        self.cmdadd_get_caller_access(caller, arg_username, arg_access, arg_flags)

    def cmdadd_get_caller_access(self, caller_username, arg_username, arg_access, arg_flags):
        access_entries_deferred = self.dbpool.runQuery("select access,flags from bot_user where username ilike %s", (caller_username,))
        access_entries_deferred.addCallback(self.cmdadd_received_caller_access_entries, caller_username, arg_username, arg_access, arg_flags)

    def cmdadd_received_caller_access_entries(self, caller_access_entries, caller_username, arg_username, arg_access, arg_flags):
        if len(caller_access_entries) < 1:
            self.log("Could not find user %s" % caller_username)
            return
        caller_access = caller_access_entries[0][0]
        if int(caller_access) <= int(arg_access):
            arg_access = caller_access-1
        self.cmdadd_get_arg_user_access(caller_username, caller_access, arg_username, arg_access, arg_flags)

    def cmdadd_get_arg_user_access(self, caller_username, caller_access, arg_username, arg_access, arg_flags):
        access_entries_deferred = self.dbpool.runQuery("select access,flags from bot_user where username ilike %s", (arg_username,))
        access_entries_deferred.addCallback(self.cmdadd_received_arg_user_access_entries, caller_username, caller_access, arg_username, arg_access, arg_flags)

    def cmdadd_received_arg_user_access_entries(self, arg_user_access_entries, caller_username, caller_access, arg_username, arg_access, arg_flags):
        if len(arg_user_access_entries) < 1:
            if arg_access < 0: #an add of less than 0 is a delete, and in this case we were unable to find the user to delete
                self.send_chat_message("There was no such user found.")
                return
            self.cmdadd_insert_new_user(caller_username, caller_access, arg_username, arg_access, arg_flags)
            return
        arg_user_access,arg_user_flags = arg_user_access_entries[0]
        if int(arg_user_access) >= int(caller_access):
            self.send_chat_message("%s does not have less access than you" % arg_username)
            return
        if int(arg_access) < 0: #an add of less than 0 is a delete
            user_entries_deferred = self.dbpool.runQuery("delete from bot_user where username ilike %s returning username,access,flags", (arg_username,))
            user_entries_deferred.addCallback(self.cmdadd_deleted_user, arg_username)
            return
        user_entries_deferred = self.dbpool.runQuery("update bot_user set access=%s,flags=%s where username=%s returning username,access,flags", (arg_access, arg_flags, arg_username))
        user_entries_deferred.addCallback(self.cmdadd_updated_user, arg_username)
        #user_entries_deferred.addErrback(self.cmdadd_add_user_failed, input_username, input_access, input_flags)

    def cmdadd_insert_new_user(self, caller_username, caller_access, arg_username, arg_access, arg_flags):
        user_entries_deferred = self.dbpool.runQuery("insert into bot_user (username,access,flags) values (%s,%s,%s) returning username,access,flags", (arg_username,arg_access,arg_flags))
        user_entries_deferred.addCallback(self.cmdadd_added_user, arg_username)

    def cmdadd_added_user(self, user_entries, attempted_username):
        if len(user_entries) < 1:
            self.log("Error adding user %s" % attempted_username)
        username,access,flags = user_entries[0]
        self.command_add_success(username, access, flags)

    def cmdadd_deleted_user(self, user_entries, attempted_username):
        if len(user_entries) < 1:
            self.log("Error deleting user %s" % attempted_username)
        username,access,flags = user_entries[0]
        self.send_chat_message("Removed user %s" % username)

    def cmdadd_updated_user(self, user_entries, attempted_username):
        if len(user_entries) < 1:
            self.log("Error updating user %s" % attempted_username)
        username,access,flags = user_entries[0]
        self.command_add_success(username, access, flags)

    def command_add_success(self, username, access, flags):
        self.send_chat_message("Set %s's access to %d" % (username, access))

    def command_whoami(self, caller, args):
        select_deferred = self.dbpool.runQuery("select access,flags from bot_user where username ilike %s", (caller,))
        select_deferred.addCallback(self.cmdwhoami_received_access_entries, caller)

    def cmdwhoami_received_access_entries(self, access_entries, username):
        if len(access_entries) < 1:
            self.send_chat_message("There was no such user found.")
            return
        access,flags = access_entries[0]
        self.send_chat_message("User %s holds rank %s." % (username, access))

    def command_whois(self, caller, args):
        if len(args) < 1:
            return
        username = args[0]
        self.command_whoami(username, [])

    def command_del(self, caller, args):
        if len(args) < 1:
            return
        username = args[0]
        self.command_add(caller, [username, -1])

    def command_roll(self, caller, args):
        if len(args) < 1:
            ubound = 100
        else:
            try:
                ubound = int(args[0])
            except ValueError:
                return
        result = random.randint(0, ubound)
        self.send_chat_message("Random number (0-%d): %d" % (ubound, result))

    def command_flip(self, caller, args):
        result = random.randint(0,1)
        output = "Heads." if result == 0 else "Tails."
        self.send_chat_message(output)
        
    def command_say(self, caller, args):
        if len(args) < 1:
            return
        message_text = " ".join(args)
        self.send_chat_message(message_text)

    def command_join(self, caller, args):
        if len(args) < 1:
            return
        channel = " ".join(args)
        self.send_chat_message("/join %s" % channel)

    def command_quit(self, caller, args):
        self.log("Received a quit command, shutting down")
        reactor.stop()

    def command_reload(self, caller, args):
        self.log("Received reload command")
        reactor.stop()
        subprocess.Popen(["python", "potbot.py"])

    def command_uptime(self, caller, args):
        self.log("Received uptime command")
        uptime = datetime.datetime.today() - self.logon_time
        uptime_seconds = int(uptime.seconds + (uptime.microseconds/1000000))
        uptime_days = uptime_seconds / 60*60*24
        uptime_seconds -= 60*60*24*uptime_days
        uptime_hours = uptime_seconds / 60*60
        uptime_seconds -= 60*60*uptime_hours
        uptime_minutes = uptime_seconds / 60
        uptime_seconds -= 60*uptime_minutes
        fmt_str = "Connection uptime: {} days, {} hours, {} minutes and " \
                  "{} seconds."
        self.send_chat_message(fmt_str.format(uptime_days,
                                              uptime_hours,
                                              uptime_minutes,
                                              uptime_seconds))

    def extract_command_text(self, message):
        message_text = message.text
        if message_text.startswith(self.config.trigger):
            return message_text[len(self.config.trigger):]
        elif message_text.startswith("/"):
            return message_text[1:]
        return ""

    def parse_command_text(self, command_text):
        split = command_text.split()
        if len(split) == 0:
            return ("", "")
        if len(split) == 1:
            return (split[0], "")
        return (split[0], split[1:])

    def log(self, msg, prepend="Potbot"):
        self.logger.debug("%s %s" % (("%s:" % prepend).ljust(20), msg))
        print "%s %s" % (("%s:" % prepend).ljust(20), msg)

    def fatal_error(self, error_msg):
        self.log("Fatal error: {0}".format(error_msg))
        if reactor.running:
            reactor.stop()

if __name__ == '__main__':
    potbot_config = PotbotConfig("config.ini")
    potbot = Potbot(potbot_config)
    potbot.logon()
    reactor.run()
