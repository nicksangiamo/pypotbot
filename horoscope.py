from twisted.internet import reactor
from twisted.internet.defer import Deferred
from twisted.internet.protocol import Protocol
from twisted.web.client import Agent
from twisted.web.http_headers import Headers
from potbotextra import PotbotService, PotbotCommand

valid_signs = [
    "aries",
    "taurus",
    "gemini",
    "cancer",
    "leo", 
    "virgo", 
    "libra",
    "scorpio",
    "sagittarius",
    "capricorn",
    "aquarius",
    "pisces"
]

class HoroscopeProtocol(Protocol):
    def __init__(self, horoscope_deferred):
        self.webpage = ""
        self.horoscope_deferred = horoscope_deferred

    def dataReceived(self, bytes):
        self.webpage += bytes

    #TODO: check the reason
    def connectionLost(self, reason):
        horoscope = self.get_horoscope(self.webpage)
        self.horoscope_deferred.callback(horoscope)
    
    def get_horoscope(self, webpage):
        strlist1 = webpage.split("<meta property=\"og:description\" content=")
        if len(strlist1) < 2:
            return
        strlist2 = strlist1[1].split("\">")
        if len(strlist2) < 2:
            return
        horoscope = strlist2[0]
        strlist1 = webpage.split("<title>")
        if len(strlist1) < 2:
            return
        strlist2 = strlist1[1].split(" - Astrology.com")
        if len(strlist2) < 2:
            return
        output_header = strlist2[0]
        output = "%s: %s ~ http://Astrology.com/" % (output_header, horoscope)
        return output

class HoroscopeService(PotbotService):
    def __init__(self, potbot):
        super(self.__class__, self).__init__(potbot)
        self.potbot.add_command("qhs", 60, self.command_qhs)
        self.initialization_success()

    def command_qhs(self, caller, args):
        if len(args) < 1:
            return
        sign = args[0].lower()
        if sign not in valid_signs:
            return
        agent = Agent(reactor)
        url = "http://www.astrology.com/horoscope/daily-quickie/%s.html" % sign
        web_response_deferred = agent.request(
            "GET",
            url,
            Headers({'User-Agent': ['Twisted Web Client']}),
            None)
        web_response_deferred.addCallback(self.cmdqhs_received_web_response)

    def cmdqhs_received_web_response(self, response):
        horoscope_deferred = Deferred()
        horoscope_deferred.addCallback(self.cmdqhs_received_horoscope)
        response.deliverBody(HoroscopeProtocol(horoscope_deferred))
        return horoscope_deferred

    def cmdqhs_received_horoscope(self, horoscope):
        self.potbot.send_chat_message(horoscope)
