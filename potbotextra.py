from abc import ABCMeta, abstractmethod
from twisted.enterprise import adbapi

class PotbotCommand(object):
    def __init__(self, name, required_access, function, required_flags = ""):
        self.name = name
        self.required_access = required_access
        self.function = function
        self.required_flags = required_flags

    def execute(self, executor, args):
        self.function(executor, args)

class PotbotService(object):
    __metaclass__ = ABCMeta

    def __init__(self, potbot):
        self.potbot = potbot

    def start(self):
        pass

    def logged_on(self, username):
        pass

    def chat_message_sent(self, msg_text):
        pass

    def emote_message_sent(self, msg_text):
        pass

    def chat_message_received(self, message):
        pass

    def whisper_message_received(self, message):
        pass

    def emote_message_received(self, message):
        pass

    def channel_join_received(self, user):
        pass

    def channel_leave_received(self, user):
        pass

    def joined_new_channel(self, channel):
        pass
    
    def info_received(self, info):
        pass
    
    def send_chat_message(self, message_text):
        self.potbot.send_chat_message(message_text)

    def get_channel_users(self):
        return self.potbot.get_channel_users()

    def initialization_success(self):
        self.log_potbot("Initialization success")

    def initialization_failure(self):
        self.log_potbot("Initialization failure")

    def log_potbot(self, msg):
        self.potbot.log(msg, prepend=self.__class__.__name__)
