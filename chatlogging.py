from potbotextra import PotbotService
from datetime import datetime
import logging
import os

class ChatLoggingService(PotbotService):
    log_directory = "./chat_logs/"

    def __init__(self, potbot):
        super(self.__class__, self).__init__(potbot)
        self.make_log_dir()
        self.new_log_file()

    def start(self):
        print "Starting chat log service"

    def new_log_file(self):
        formatter = logging.Formatter(
                "%(asctime)s %(message)s",
                "(%H:%M:%S)")
        log_filename = self.get_log_filename()
        handler = logging.FileHandler(log_filename, mode="a")
        handler.setFormatter(formatter)
        handler.setLevel(logging.INFO)
        self.logger = logging.getLogger(__name__)
        self.logger.addHandler(handler)
        self.logger.setLevel(logging.INFO)
        self.log_creation_hour = datetime.now().hour
        self.initialization_success()

    def make_log_dir(self):
        if not os.path.exists(self.__class__.log_directory):
            os.makedirs(self.__class__.log_directory)

    def get_log_filename(self):
        path = self.__class__.log_directory
        name = datetime.now().strftime("%Y-%m-%d")  + ".log"
        return path + name
    
    def logged_on(self, username):
        self.log("-- Logged on as %s --" % username)

    def chat_message_sent(self, msg_text):
        self.log("<%s> %s" % (self.potbot.config.username, msg_text))

    def emote_message_sent(self, msg_text):
        msg_text = " ".join(msg_text.split()[1:])
        self.log("<%s %s>" % (self.potbot.config.username, msg_text))

    def chat_message_received(self, message):
        username = message.user.username
        msg_text = message.text
        self.log("<%s> %s" % (username, msg_text))

    def whisper_message_received(self, message):
        username = message.user.username
        msg_text = message.text
        self.log("<From %s> %s" % (username, msg_text))

    def emote_message_received(self, message):
        username = message.user.username
        msg_text = message.text
        self.log("<%s %s>" % (username, msg_text))

    def channel_join_received(self, user):
        username = user.username
        self.log("-- %s has joined the channel --" % username)

    def channel_leave_received(self, user):
        username = user.username
        self.log("-- %s has left the channel --" % username)

    def joined_new_channel(self, channel):
        self.log("-- Joined channel %s --" % channel)

    def info_received(self, info):
        self.log(info)

    def log(self, msg):
        if datetime.now().hour < self.log_creation_hour:
            self.new_log_file()
        self.logger.info(msg)
