from twisted.internet import reactor
from potbotextra import PotbotService
from bnutil import strip_username

CHALLENGE_ACCEPT_TIMEOUT = 15
GAME_TIMEOUT = 15
VALID_SELECTIONS = ["rock", "paper", "scissors"]

class Challenge(object):
    def __init__(self, challenger, challengee, timer):
        self.challenger = challenger
        self.challengee = challengee
        self.timer = timer

class Game(object):
    def __init__(self, challenger, challengee, timer):
        self.challenger = challenger
        self.challengee = challengee
        self.timer = timer
        self.challenger_selection = None
        self.challengee_selection = None

class RPSService(PotbotService):
    def __init__(self, potbot):
        super(self.__class__, self).__init__(potbot)
        self.issued_challenges = {}
        self.active_games = {}
        self.potbot.add_command("rps", 50, self.command_rps)
        self.initialization_success()

    def command_rps(self, caller, args):
        if len(args) < 1:
            self.log_potbot("Not enough args for RPS command")
            return
        challenger = caller
        challengee = args[0]
        if challenger.lower() == challengee.lower():
            self.send_chat_message("You can't challenge yourself.")
            return
        challengee_in_channel = False
        for channel_user in self.get_channel_users():
            if strip_username(challengee.lower()) == \
                    strip_username(channel_user.lower()):
                if challengee_in_channel:
                    challengee = strip_username(channel_user)
                    break
                challengee = channel_user
                challengee_in_channel = True
        if not challengee_in_channel:
            self.send_chat_message("Can't see {} in the channel." \
                                   .format(strip_username(challengee)))
            return
        if challengee in self.issued_challenges:
            current_challenger = self.issued_challenges[challengee].challenger
            self.send_chat_message("{} is already being challenged by {}" \
                                   .format(strip_username(challengee),
                                           strip_username(current_challenger)))
            return
        self.send_chat_message("{}, you have been challenged by {} to a game "
                               "of Rock-paper-scissors. Do you accept? "
                               "Type yes/no within the next {} seconds." \
                               .format(strip_username(challengee),
                                       strip_username(challenger),
                                       CHALLENGE_ACCEPT_TIMEOUT))
        challenge_timer = reactor.callLater(CHALLENGE_ACCEPT_TIMEOUT,
                                            self.challenge_expired,
                                            challengee)
        issued_challenge = Challenge(challenger, challengee, challenge_timer)
        self.issued_challenges[challengee] = issued_challenge

    def challenge_expired(self, challengee):
        challenger = self.issued_challenges[challengee].challenger
        challengee = self.issued_challenges[challengee].challengee
        self.send_chat_message("The cowardly {} did not accept {}'s "
                               "challenge.".format(strip_username(challengee), 
                                                   strip_username(challenger)))
        del self.issued_challenges[challengee]

    def game_expired(self, challengee):
        active_game = self.active_games[challengee]
        challenger = active_game.challenger
        challengee = active_game.challengee
        challenger_selection = active_game.challenger_selection
        challengee_selection = active_game.challengee_selection
        if not challenger_selection and not challengee_selection:
            self.send_chat_message("Time's up... Neither {} nor {} made a "
                                   "selection." \
                                   .format(strip_username(challenger),
                                           strip_username(challengee)))
        else:
            if not challenger_selection:
                non_selector = challenger
            else:
                non_selector = challengee
            self.send_chat_message("Time's up... {} did not make a selection." \
                                   .format(strip_username(non_selector)))
        del self.active_games[challengee]

    def chat_message_received(self, message):
        username = message.user.username
        msg_text = message.text
        for challengee in self.issued_challenges:
            if username.lower() == challengee.lower():
                issued_challenge = self.issued_challenges[challengee]
                if msg_text.lower().strip() in ["yes"]:
                    self.challenge_accepted(issued_challenge)
                elif msg_text.lower().strip() in ["no"]:
                    self.challenge_declined(issued_challenge)
                return

    def whisper_message_received(self, message):
        username = message.user.username
        msg_text = message.text
        for active_game in self.active_games.values():
            challenger = active_game.challenger
            challengee = active_game.challengee
            if username.lower() not in [challenger.lower(), challengee.lower()]:
                continue
            selection = msg_text.lower().strip()
            if selection not in VALID_SELECTIONS:
                continue
            if username.lower() == challenger.lower():
                active_game.challenger_selection = selection
            elif username.lower() == challengee.lower():
                active_game.challengee_selection = selection
            if (active_game.challenger_selection in VALID_SELECTIONS and 
                    active_game.challengee_selection in VALID_SELECTIONS):
                self.game_selections_made(active_game)

    def challenge_accepted(self, issued_challenge):
        challenger = issued_challenge.challenger
        challengee = issued_challenge.challengee
        challenge_timer = issued_challenge.timer
        self.cancel_timer(challenge_timer)
        del self.issued_challenges[challengee]
        self.send_chat_message("{} has accepted {}'s challenge. Please "
                               "whisper your selections to me within the "
                               "next {} seconds." \
                               .format(strip_username(challengee),
                                       strip_username(challenger),
                                       GAME_TIMEOUT))
        game_timer = reactor.callLater(GAME_TIMEOUT,
                                       self.game_expired,
                                       challengee)
        active_game = Game(challenger, challengee, game_timer)
        self.active_games[challengee] = active_game

    def challenge_declined(self, issued_challenge):
        challenger = issued_challenge.challenger
        challengee = issued_challenge.challengee
        challenge_timer = issued_challenge.timer
        self.cancel_timer(challenge_timer)
        del self.issued_challenges[challengee]
        self.send_chat_message("The cowardly {} declined {}'s challenge." \
                               .format(strip_username(challengee),
                                       strip_username(challenger)))

    def game_selections_made(self, active_game):
        challenger = active_game.challenger
        challengee = active_game.challengee
        game_timer = active_game.timer
        challenger_selection = active_game.challenger_selection
        challengee_selection = active_game.challengee_selection
        self.cancel_timer(game_timer)
        del self.active_games[challengee]
        if challenger_selection == challengee_selection:
            self.send_chat_message(
                    "{} and {} both selected {}... The game "
                    "is a tie!" \
                    .format(strip_username(challengee),
                            strip_username(challenger),
                            strip_username(challenger_selection)))
        else:
            winner,loser = self.get_winner_loser(challenger,
                                                 challengee,
                                                 challenger_selection,
                                                 challengee_selection)
            self.send_chat_message("{} selected {}, and {} selected {}... "
                                   "{} wins!" \
                                    .format(strip_username(challenger),
                                            challenger_selection,
                                            strip_username(challengee),
                                            challengee_selection,
                                            strip_username(winner)))

    def get_winner_loser(self,
                         player1,
                         player2,
                         player1_selection,
                         player2_selection):
        if player1_selection == VALID_SELECTIONS[0]:
            if player2_selection == VALID_SELECTIONS[1]:
                return (player2, player1)
            elif player2_selection == VALID_SELECTIONS[2]:
                return (player1, player2)
        elif player1_selection == VALID_SELECTIONS[1]:
            if player2_selection == VALID_SELECTIONS[0]:
                return (player1, player2)
            elif player2_selection == VALID_SELECTIONS[2]:
                return (player2, player1)
        elif player1_selection == VALID_SELECTIONS[2]:
            if player2_selection == VALID_SELECTIONS[0]:
                return (player2, player1)
            elif player2_selection == VALID_SELECTIONS[1]:
                return (player1, player2)
        return ("", "")

    def cancel_timer(self, timer):
        try:
            timer.cancel()
        except (TwistedAlreadyCancelled, TwistedAlreadyCalled) as e:
            pass
