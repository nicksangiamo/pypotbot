#!/usr/bin/python

import subprocess
import datetime

directory = "/home/ubuntu/potbot/db_backup/"

def get_filename(date, filetype):
    date_string = date.strftime("%y-%m-%d")
    fmt_str = "{0}{1}/{2}.sql"
    return fmt_str.format(directory, filetype, date_string)

def get_potbot_filename(date):
    return get_filename(date, "potbot")

def get_potbot_trivia_filename(date):
    return get_filename(date, "potbot_trivia")

today = datetime.datetime.today()
potbot_filename = get_potbot_filename(today)
potbot_trivia_filename = get_potbot_trivia_filename(today)
command1 = "pg_dump -f {0} potbot".format(potbot_filename)
command2 = "pg_dump -f {0} potbot_trivia".format(potbot_trivia_filename)
subprocess.call(command1.split())
subprocess.call(command2.split())
if today.day != 1:
    yesterday = today - datetime.timedelta(days=1)
    potbot_filename = get_potbot_filename(yesterday)
    potbot_trivia_filename = get_potbot_trivia_filename(yesterday)
    command1 = "rm -f {0}".format(potbot_filename)
    command2 = "rm -f {0}".format(potbot_trivia_filename)
    subprocess.call(command1.split())
    subprocess.call(command2.split())
