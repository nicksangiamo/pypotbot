from struct import pack, unpack
from abc import ABCMeta, abstractmethod

class BnUser(object):
    def __init__(self, username, flags=0, ping=0):
        self.username = username
        self.flags = flags
        self.ping = ping

class BnMessage(object):
    __metaclass__ = ABCMeta

    def __init__(self, user, text):
        self.user = user
        self.text = text

class BnChatMessage(BnMessage):
    def __init__(self, user, message):
        super(self.__class__, self).__init__(user, message)

class BnWhisperMessage(BnMessage):
    def __init__(self, user, message):
        super(self.__class__, self).__init__(user, message)

class BnChannel(object):
    def __init__(self, name, users=[]):
        self.name = name
        self.users = users

    def num_users(self):
        return len(self.users)

    def add_user(self, user):
        if user not in self.users:
            self.users.append(user)

    def remove_user(self, user):
        if user in self.users:
            self.users.remove(user)

#TODO: do we care if data_index surpasses len(data)?
class DataBuffer(object):
    def __init__(self, data):
        self.data = data
        self.data_index = 0
        self.data_len = len(self.data)

    def extract_bytes(self, num_bytes):
        if num_bytes < 0:
            return ""
        extraction = self.data[self.data_index:self.data_index+num_bytes]
        self.data_index += num_bytes
        return extraction

    def extract_byte(self):
        try:
            byte, = unpack("B", self.data[self.data_index:self.data_index+1])
        except Exception as exception:
            return 0
        self.data_index += 1
        return byte 
    
    def extract_word(self):
        try:
            word, = unpack("<H", self.data[self.data_index:self.data_index+2])
        except Exception as exception:
            return 0
        self.data_index += 2
        return word
    
    def extract_dword(self):
        try:
            dword, = unpack("<L", self.data[self.data_index:self.data_index+4])
        except Exception as exception:
            return 0
        self.data_index += 4
        return dword 
     
    def extract_dword_bigendian(self):
        try:
            dword, = unpack(">L", self.data[self.data_index:self.data_index+4])
        except Exception as exception:
            return 0
        self.data_index += 4
        return dword 
   
    def extract_dword_tuple(self, num_dwords):
        dwords = ()
        for i in range(num_dwords):
            dwords += self.extract_dword(),
        return dwords
   
    def extract_dword_tuple(self, num_dwords):
        dwords = ()
        for i in range(num_dwords):
            dwords += self.extract_dword_bigendian(),
        return dwords

    def extract_cstring(self):
        extraction = self.data[self.data_index:].split("\0", 1)[0]
        self.data_index += len(extraction) + 1
        return extraction

    def unprocessed_bytes(self):
        return self.data_len - self.data_index

    def size(self):
        return self.data_len

class CdKeyInfo(object):
    def __init__(
            self,
            key_length,
            key_product_value,
            key_public_value,
            unknown,
            hashed_key_data):
        self.key_length = key_length
        self.key_product_value = key_product_value
        self.key_public_value = key_public_value
        self.unknown = unknown
        self.hashed_key_data = hashed_key_data
    def to_buffer(self):
        buff = pack("<4L",
                     self.key_length,
                     self.key_product_value,
                     self.key_public_value,
                     self.unknown)
        buff += pack(">5L", *self.hashed_key_data)
        return buff
        #return pack(
        #        "<9L",
        #        self.key_length,
        #        self.key_product_value,
        #        self.key_public_value,
        #        self.unknown,
        #        *self.hashed_key_data)

def to_hex(string):
    return ':'.join(char.encode('hex') for char in string)

def is_bnet_command(message):
    if message[0] != "/":
        return False
    if message == "/":
        return False
    cmd = message.split()[0][1:]
    if cmd in ["me", "emote"]:
        return False
    return True

def is_emote(message):
    if message[0:3].lower() == "/me":
        return True
    if message[0:6].lower() == "/emote":
        return True
    return False

def strip_username(username):
    split_index = username.rfind("#")
    if split_index == -1:
        return username
    return username[:split_index]

def get_user_realm(username, default="USEast"):
    split_index = username.rfind("#")
    if split_index == -1:
        return default
    return username[split_index+1:]
