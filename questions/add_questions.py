#!/usr/bin/python
import sys
import psycopg2

class QuestionParser(object):
    def __init__(self, filename):
        self.question_file = open(filename)
        self.dbconn = psycopg2.connect("dbname=potbot_trivia")
        self.dbconn.autocommit = True
        self.dbcur = self.dbconn.cursor()

    def add_questions(self):
        for qa in self.question_file:
            qa_split = qa.split("*")
            if len(qa_split) != 2:
                print "Error parsing line: " + qa
            q = qa_split[0].strip()
            a = qa_split[1].strip()
            self.add_question(q, a)

    def add_question(self, question, answer):
        #print "Q: %s A: %s" % (question, answer)
        try:
            self.dbcur.execute("insert into question (question,answer) values (%s,%s) returning id,question", (question, answer))
        except Exception as e:
            print e.message
        try:
            print self.dbcur.fetchone()
        except Exception as e:
            print e.message

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: %s <filename>" % sys.argv[0]
        sys.exit(1)
    filename = sys.argv[1]
    parser = QuestionParser(filename)
    parser.add_questions()
