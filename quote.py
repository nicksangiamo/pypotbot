from datetime import datetime
from potbotextra import PotbotService, PotbotCommand

class ChatMessage(object):
    def __init__(self, username, text):
        self.username = username
        self.text = text
        self.time = datetime.now()

class QuoteService(PotbotService):
    def __init__(self, potbot):
        super(self.__class__, self).__init__(potbot)
        self.dbpool = self.potbot.dbpool
        self.recent_chat = []
        self.max_recent_chat = 21
        self.potbot.add_command("aquote", 95, self.command_aquote)
        self.potbot.add_command("quote", 60, self.command_quote)
        self.initialization_success()

    def start(self):
        self.log_potbot("Starting quotes")

    def chat_message_sent(self, msg_text):
        self.update_recent_chat(self.potbot.config.username, msg_text)

    def chat_message_received(self, message):
        username = message.user.username
        msg_text = message.text
        self.update_recent_chat(username, msg_text)

    def update_recent_chat(self, username, msg_text):
        self.recent_chat.append(ChatMessage(username, msg_text))
        if len(self.recent_chat) > self.max_recent_chat:
            self.recent_chat.pop(0)

    def commands_enabled(self):
        self.dbpool = self.potbot.dbpool
        return self.dbpool is not None

    def command_aquote(self, caller, args):
        if not self.commands_enabled():
            self.log_potbot("Got aquote command but commands aren't enabled")
            return
        if len(args) < 1:
            self.log_potbot("Got aquote command but not enough args")
            return
        try:
            arg_index = int(args[0])
        except ValueError:
            self.log_potbot("Couldn't convert arg to int")
            return
        if arg_index < 0 or arg_index > self.max_recent_chat:
            self.log_potbot("Arg out of range")
            return
        true_index = len(self.recent_chat) - arg_index - 1
        if true_index >= len(self.recent_chat) or true_index < 0:
            self.log_potbot("Index out of range")
            return
        chat_entry = self.recent_chat[true_index]
        quoter = caller
        quotee = chat_entry.username
        quote = chat_entry.text
        quote_time = chat_entry.time
        quote_entries_deferred = self.dbpool.runQuery("insert into quote (quoter,quotee,quote,quote_time) values (%s,%s,%s,%s) returning quoter,quotee,quote,quote_time", (quoter, quotee, quote, quote_time)) 
        quote_entries_deferred.addCallback(self.cmdaquote_inserted_quote_entries)

    def cmdaquote_inserted_quote_entries(self, quote_entries):
        if len(quote_entries) < 1:
            return
        self.potbot.send_chat_message("Quote added!")

    def command_quote(self, caller, args):
        if not self.commands_enabled():
            return
        num_quote_entries_deferred = self.dbpool.runQuery("select count(*) from quote")
        num_quote_entries_deferred.addCallback(self.cmdquote_received_num_quote_entries)

    def cmdquote_received_num_quote_entries(self, num_quote_entries):
        if len(num_quote_entries) < 1:
            self.log_potbot("Unable to retrieve the number of quotes in the db")
        num_quotes = num_quote_entries[0][0]
        if num_quotes == 0:
            self.potbot.send_chat_message("No quotes have been added yet.")
            return
        quote_entries_deferred = self.dbpool.runQuery("select quoter,quotee,quote,quote_time from quote offset floor(random()*%d) limit 1" % num_quotes)
        quote_entries_deferred.addCallback(self.cmdquote_received_quote_entries)

    def cmdquote_received_quote_entries(self, quote_entries):
        if len(quote_entries) < 1:
            self.log_potbot("Did not receive a quote back from the db")
        quoter,quotee,quote,quote_time = quote_entries[0]
        the_time = quote_time.strftime("%-I:%M:%S %p on %-m/%-d/%y")
        output = "Quote: (%s) \"%s\" ~ %s" % (the_time, quote, quotee)
        self.potbot.send_chat_message(output)
