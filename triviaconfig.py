from ConfigParser import SafeConfigParser

class TriviaConfigError(LookupError):
    pass

class TriviaConfig(object):
    def __init__(self, filename):
        self.filename = filename
        self.parser = SafeConfigParser()
        self.non_attributes = ["non_attributes", "filename", "parser"]
        self.num_hints = 0
        self.hint_interval = 0
        self.hint_character = ""
        self.hint_probability = 0.0
        self.question_interval = 0.0
        self.streak_threshold = 0
        self.global_emote = False
        self.idle_stats = False
        self.idle_stats_interval = 0.0
        self.read_file()

    def read_file(self):
        filesRead = self.parser.read(self.filename)
        if self.filename not in filesRead:
            raise IOError("{} not found".format(self.filename))
        for attribute in self.__dict__:
            if attribute in self.non_attributes:
                continue
            self.get_value(attribute)

    def get_value(self, attribute):
        if self.parser.has_option("main", attribute):
            attr_type = type(getattr(self, attribute))
            if attr_type is str:
                the_value = self.parser.get("main", attribute)
            elif attr_type is int:
                the_value = self.parser.getint("main", attribute)
                if the_value < 0:
                    err_msg = "Invalid value for %s: %d" % (
                            attribute, the_value)
                    raise TriviaConfigError(err_msg)
            elif attr_type is float:
                the_value = self.parser.getfloat("main", attribute)
                if the_value < 0:
                    err_msg = "Invalid value for %s: %f" % (
                            attribute, the_value)
                    raise TriviaConfigError(err_msg)
            elif attr_type is bool:
                the_string_value = self.parser.get("main", attribute)
                if the_string_value.lower() == "on":
                    the_value = True
                elif the_string_value.lower() == "off":
                    the_value = False
                else:
                    err_msg = "Attribute %s must be \"on\" or \"off\""
                    raise TriviaConfigError(err_msg % attribute)
            else:
                err_msg = "Internal error: Looking for invalid type for %s: %s"
                raise TriviaConfigError(err_msg % (attribute, attr_type))
            setattr(self, attribute, the_value)
        else:
            raise TriviaConfigError(attribute)

    def set_value(self, attribute, value):
        if attribute not in self.__dict__ or attribute in self.non_attributes:
            return
        if type(value) is bool:
            if value:
                value = "on"
            else:
                value = "off"
        self.parser.set("main", attribute, str(value))
        the_config_file = open(self.filename ,"wt")
        self.parser.write(the_config_file)
