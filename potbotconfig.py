from ConfigParser import SafeConfigParser

DEFAULT_BNLS_ADDRESS = "phix.no-ip.org"
DEFAULT_BNLS_PORT = 9367
DEFAULT_BNCS_ADDRESS = "useast.battle.net"
DEFAULT_BNCS_PORT = 6112
DEFAULT_DB_ADDRESS = "localhost"
DEFAULT_DB_PORT = 5432
DEFAULT_HOME_CHANNEL = "public chat 1"
DEFAULT_TRIGGER = "$"

class PotbotConfigError(LookupError):
    pass

class PotbotConfig(object):
    def __init__(self, filename):
        self.parser = SafeConfigParser()
        self.bnls_address = DEFAULT_BNLS_ADDRESS
        self.bnls_port = DEFAULT_BNLS_PORT
        self.bncs_address = DEFAULT_BNCS_ADDRESS
        self.bncs_port = DEFAULT_BNCS_PORT
        self.db_address = DEFAULT_DB_ADDRESS
        self.db_port = DEFAULT_DB_PORT
        self.home_channel = DEFAULT_HOME_CHANNEL
        self.trigger = DEFAULT_TRIGGER
        self.username = None
        self.password = None
        self.product = None
        self.cdkey = None
        self.owner = None
        self.read_file(filename)

    def read_file(self, filename):
        filesRead = self.parser.read(filename)
        if filename not in filesRead:
            raise IOError("{} not found".format(filename))
        if self.parser.has_option("main", "bnls_address"):
            self.bnls_address = self.parser.get("main", "bnls_address")
        if self.parser.has_option("main", "bnls_port"):
            self.bnls_port = self.parser.getint("main", "bnls_port")
        if self.parser.has_option("main", "bncs_address"):
            self.bncs_address = self.parser.get("main", "bncs_address")
        if self.parser.has_option("main", "bncs_port"):
            self.bncs_port = self.parser.getint("main", "bncs_port")
        if self.parser.has_option("main", "db_address"):
            self.db_address = self.parser.get("main", "db_address")
        if self.parser.has_option("main", "db_port"):
            self.db_port = self.parser.getint("main", "db_port")
        if self.parser.has_option("main", "home_channel"):
            self.home_channel = self.parser.get("main", "home_channel")
        if self.parser.has_option("main", "trigger"):
            self.trigger = self.parser.get("main", "trigger")
        if self.parser.has_option("main", "username"):
            self.username = self.parser.get("main", "username")
        else:
            raise PotbotConfigError("username")
        if self.parser.has_option("main", "password"):
            self.password = self.parser.get("main", "password")
        else:
            raise PotbotConfigError("password")
        if self.parser.has_option("main", "product"):
            self.product = self.parser.get("main", "product")
        else:
            raise PotbotConfigError("product")
        if self.parser.has_option("main", "cdkey"):
            self.cdkey = self.parser.get("main", "cdkey")
        else:
            raise PotbotConfigError("cdkey")
        if self.parser.has_option("main", "owner"):
            self.owner = self.parser.get("main", "owner")
        else:
            raise PotbotConfigError("owner")
