import re
from twisted.internet import reactor
from twisted.internet.defer import Deferred
from twisted.internet.protocol import Protocol
from twisted.web.client import Agent, RedirectAgent
from twisted.web.http_headers import Headers
from HTMLParser import HTMLParser
from potbotextra import PotbotService, PotbotCommand

class UrbanDictionaryProtocol(Protocol):
    def __init__(self, definition_deferred):
        self.webpage = ""
        self.definition_deferred = definition_deferred
        self.initialization_success()

    def dataReceived(self, bytes):
        self.webpage += bytes
        self.loseConnection()

    #TODO: check the reason
    def connectionLost(self, reason):
        definition = self.get_definition(self.webpage)
        self.definition_deferred.callback(definition)

    def get_definition(self, webpage):
        parser = UrbanDictionaryParser()
        parser.feed(webpage)
        definition = parser.meaning.strip()
        definition = definition.replace("\r\n", " ")
        definition = definition.replace("\n", " ")
        return definition
        #strlist1 = webpage.split("<div class='meaning'>")
        #if len(strlist1) < 2:
        #    return
        #strlist2 = strlist1[1].split("</div>")
        #if len(strlist2) < 2:
        #    return
        #definition = strlist2[0].strip()
        #return definition

class UrbanDictionaryParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.in_meaning_tag = False
        self.meaning = ""
        self.interior_divs = 0
        self.done_parsing = False

    def handle_starttag(self, tag, attrs):
        if self.done_parsing:
            return
        if tag != "div":
            return
        if self.in_meaning_tag:
            self.interior_divs += 1
            return
        for attr in attrs:
            if attr[0] != "class":
                continue
            if attr[1] != "meaning":
                continue
            self.in_meaning_tag = True

    def handle_data(self, data):
        if self.done_parsing:
            return
        if not self.in_meaning_tag:
            return
        self.meaning += data

    def handle_endtag(self, tag):
        if self.done_parsing:
            return
        if not self.in_meaning_tag:
            return
        if tag != "div":
            return
        self.interior_divs -= 1
        if self.interior_divs > 0:
            return
        self.in_meaning_tag = False
        self.done_parsing = True

class UrbanDictionaryService(PotbotService):
    def __init__(self, potbot):
        super(self.__class__, self).__init__(potbot)
        self.term = ""
        self.potbot.add_command("ud", 60, self.command_ud)

    def command_ud(self, caller, args):
        if len(args) < 1:
            return
        self.term = " ".join(args).strip()
        self.term = re.sub(" +", " ", self.term)
        agent = RedirectAgent(Agent(reactor))
        url = "http://www.urbandictionary.com/define.php?term={}" \
              .format(self.term.replace(" ", "+"))
        web_response_deferred = agent.request(
                "GET",
                url,
                Headers({'User-Agent': ['Twisted Web Client']}),
                None)
        web_response_deferred.addCallback(self.cmdud_received_web_response)

    def cmdud_received_web_response(self, response):
        definition_deferred = Deferred()
        definition_deferred.addCallback(self.cmdud_received_definition)
        response.deliverBody(UrbanDictionaryProtocol(definition_deferred))
        return definition_deferred

    def cmdud_received_definition(self, definition):
        cmd_response = "{}: {} ~ http://urbandictionary.com/" \
                       .format(self.term, definition)
        self.potbot.send_chat_message(cmd_response)
