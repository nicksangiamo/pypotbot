set client_min_messages to warning;

drop table if exists bot_user cascade;
create table bot_user(
	id serial,
	username varchar(30) not null unique,
    access integer not null,
    flags varchar(20),
	primary key(id)
);

drop table if exists quote cascade;
create table quote(
    id serial,
    quoter varchar(30) not null,
    quotee varchar(30) not null,
    quote varchar(224) not null,
    quote_time timestamp without time zone not null,
    primary key(id)
);

drop table if exists definition cascade;
create table definition(
    id serial,
    term varchar(20) not null,
    meaning varchar(500) not null,
    creator varchar(30) not null,
    creation_date timestamp without time zone not null,
    last_modifier varchar(30),
    last_modified_date timestamp without time zone,
    primary key(id)
);
