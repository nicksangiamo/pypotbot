set client_min_messages to warning;

drop table if exists question cascade;
create table question(
	id serial,
	question varchar(500) not null,
	answer varchar(500) not null,
	point_multiplier numeric(10, 2) default 1.00,
	primary key(id)
);

drop table if exists player cascade;
create table player(
	id serial,
	username varchar(30) unique not null,
    gender varchar(30) not null default 'male',
	primary key(id)
);

drop table if exists streak cascade;
create table streak(
	id serial,
	player_id integer references player(id),
	length integer not null,
	primary key(id)
);

drop table if exists question_instance cascade;
create table question_instance(
	id serial,
	question_id integer references question(id),
	ask_time timestamp without time zone not null,
	primary key(id)
);

drop table if exists correct_answer_instance cascade;
create table correct_answer_instance(
	id serial,
	question_instance_id integer references question_instance(id),
	player_id integer references player(id),
	streak_id integer references streak(id),
	answer_time timestamp without time zone not null,
	points_earned numeric(10, 2),
	primary key(id)
);
