from twisted.internet.protocol import Protocol, ClientFactory
from struct import pack, unpack
from abc import ABCMeta, abstractmethod
from bnutil import *

byte_size = 1
word_size = 2
dword_size = 4

class BnlsPacketIds(object):
    id_null = 0x00
    id_cdkey = 0x01
    id_logonchallenge = 0x02
    id_logonproof = 0x03
    id_createaccount = 0x04
    id_changechallenge = 0x05
    id_changeproof = 0x06
    id_upgradechallenge = 0x07
    id_upgradeproof = 0x08
    id_versioncheck = 0x09
    id_confirmlogon = 0x0a
    id_hashdata = 0x0b
    id_cdkey_ex = 0x0c
    id_choosenlsrevision = 0x0d
    id_authorize = 0x0e
    id_authorizeproof = 0x0f
    id_requestversionbyte = 0x10

class BnlsPacketNames(object):
    names = {
        BnlsPacketIds.id_null : "BNLS_NULL",
        BnlsPacketIds.id_cdkey : "BNLS_CDKEY",
        BnlsPacketIds.id_logonchallenge : "BNLS_LOGONCHALLENGE",
        BnlsPacketIds.id_logonproof : "BNLS_LOGONPROOF",
        BnlsPacketIds.id_createaccount : "BNLS_CREATEACCOUNT",
        BnlsPacketIds.id_changechallenge : "BNLS_CHANGECHALLENGE",
        BnlsPacketIds.id_changeproof : "BNLS_CHANGEPROOF",
        BnlsPacketIds.id_upgradechallenge : "BNLS_UPGRADECHALLENGE",
        BnlsPacketIds.id_upgradeproof : "BNLS_UPGRADEPROOF",
        BnlsPacketIds.id_versioncheck : "BNLS_VERSIONCHECK",
        BnlsPacketIds.id_confirmlogon : "BNLS_CONFIRMLOGON",
        BnlsPacketIds.id_hashdata : "BNLS_HASHDATA",
        BnlsPacketIds.id_cdkey_ex : "BNLS_CDKEY_EX",
        BnlsPacketIds.id_choosenlsrevision : "BNLS_CHOOSENLSREVISION",
        BnlsPacketIds.id_authorize : "BNLS_AUTHORIZE",
        BnlsPacketIds.id_authorizeproof : "BNLS_AUTHORIZEPROOF",
        BnlsPacketIds.id_requestversionbyte : "BNLS_REQUESTVERSIONBYTE",
    }
    
    @staticmethod
    def get(packet_id):
        if packet_id in BnlsPacketNames.names:
            return BnlsPacketNames.names[packet_id] 
        else:
            return "BNLS_UNKNOWN_PACKET"

class BnlsProductIds(object):
    id_starcraft = 0x01
    id_starcraft_bw = 0x02
    id_warcraft_ii = 0x03
    id_diablo_ii = 0x04
    id_diablo_ii_lod = 0x05
    id_starcraft_japanese = 0x06
    id_warcraft_iii = 0x07
    id_warcraft_iii_ft = 0x08
    diablo_retail = 0x09
    diablo_shareware = 0x0a
    starcraft_shareware = 0x0b
    warcraft_iii_demo = 0x0c


class BnlsClientPacket(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_message_buffer():
        pass

    def to_buffer(self):
        self.message_buffer = self.get_message_buffer()
        self.length = 3 + len(self.message_buffer)
        return pack("<HB", self.length, self.id) + self.message_buffer
    
class BnlsServerPacket(object):
    __metaclass__ = ABCMeta

class BnlsClientAuthorizePacket(BnlsClientPacket):
    def __init__(self, bot_id):
        self.id = BnlsPacketIds.id_authorize
        self.bot_id = bot_id + "\0"
    
    def get_message_buffer(self):
        return self.bot_id

class BnlsServerAuthorizePacket(BnlsServerPacket):
    def __init__(self, server_code):
        self.id = BnlsPacketIds.id_authorize
        self.server_code = server_code

class BnlsClientAuthorizeProofPacket(BnlsClientPacket):
    def __init__(self, checksum):
        self.id = BnlsPacketIds.id_authorizeproof
        self.checksum = checksum
    
    def get_message_buffer(self):
        return pack("<L", self.checksum)

class BnlsServerAuthorizeProofPacket(BnlsServerPacket):
    def  __init__(self, status_code):
        self.id = BnlsPacketIds.id_authorizeproof
        self.status_code = status_code

class BnlsClientRequestVersionBytePacket(BnlsClientPacket):
    def __init__(self, product_id):
        self.id = BnlsPacketIds.id_requestversionbyte
        self.product_id = product_id

    def get_message_buffer(self):
        return pack("<L", self.product_id)

class BnlsServerRequestVersionBytePacket(BnlsServerPacket):
    def __init__(self, product_id, version_byte=0):
        self.id = BnlsPacketIds.id_requestversionbyte
        self.product_id = product_id
        self.version_byte = version_byte

class BnlsClientVersionCheckPacket(BnlsClientPacket):
    def __init__(self, product_id, version_dll_digit, checksum_formula):
        self.id = BnlsPacketIds.id_versioncheck
        self.product_id = product_id
        self.version_dll_digit = version_dll_digit
        self.checksum_formula = checksum_formula + "\0"

    def get_message_buffer(self):
        message_buffer = pack("<2L", self.product_id, self.version_dll_digit)
        message_buffer += self.checksum_formula
        return message_buffer

class BnlsServerVersionCheckPacket(BnlsServerPacket):
    def __init__(self, success, exe_version=0, exe_checksum=0, stat_string=""):
        self.id = BnlsPacketIds.id_versioncheck
        self.success = success
        self.exe_version = exe_version
        self.exe_checksum = exe_checksum
        self.stat_string = stat_string

class BnlsClientCdKeyPacket(BnlsClientPacket):
    def __init__(self, server_token, cdkey):
        self.id = BnlsPacketIds.id_cdkey
        self.server_token = server_token
        self.cdkey = cdkey + "\0"

    def get_message_buffer(self):
        return pack("<L", self.server_token) + self.cdkey

class BnlsServerCdKeyPacket(BnlsServerPacket):
    def __init__(
            self,
            success,
            client_token = 0,
            cdkey_length = 0,
            cdkey_product_value = 0,
            cdkey_public_value = 0,
            unknown = 0,
            cdkey_hash = (0, 0, 0, 0, 0)):
        self.id = BnlsPacketIds.id_cdkey
        self.client_token = client_token
        self.cdkey_length = cdkey_length
        self.cdkey_product_value = cdkey_product_value
        self.cdkey_public_value = cdkey_public_value
        self.unknown = unknown
        self.cdkey_hash = cdkey_hash

# TODO: this is currently a "watered down" version to match the regular
# BnlsClientCdKeyPacket
class BnlsClientCdKeyExPacket(BnlsClientPacket):
    def __init__(self, server_token, cdkey):
        self.id = BnlsPacketIds.id_cdkey_ex
        self.cookie = 0x78563412
        self.server_token = server_token
        self.cdkey = cdkey + "\0"

    def get_message_buffer(self):
        # TODO: add other flags values aside from 0x01, 0x08
        message_buffer =  pack("<LBLL",
                               self.cookie,
                               1,
                               0x01 | 0x08,
                               self.server_token)
        message_buffer += self.cdkey
        return message_buffer

class BnlsServerCdKeyExPacket(BnlsServerPacket):
    def __init__(self,
                 cookie,
                 num_requested_keys,
                 num_encrypted_keys,
                 bit_mask,
                 client_session_key,
                 cdkey_data):
        self.id = BnlsPacketIds.id_cdkey_ex
        self.cookie = cookie
        self.num_requested_keys = num_requested_keys
        self.num_encrypted_keys = num_encrypted_keys
        self.bit_mask = bit_mask
        self.client_session_key = client_session_key
        self.cdkey_data = cdkey_data

class BnlsClientHashDataPacket(BnlsClientPacket):
    def __init__(
            self,
            data_size,
            flags,
            data,
            client_key=0,
            server_key=0,
            cookie=0):
        self.id = BnlsPacketIds.id_hashdata
        self.data_size = data_size
        self.flags = flags if flags in [0x00, 0x01, 0x02, 0x04] else 0x00
        self.data = data
        self.client_key = client_key
        self.server_key = server_key

    def get_message_buffer(self):
        message_buffer = pack("<2L", self.data_size, self.flags) + self.data
        if self.flags & 0x02:
            message_buffer += pack("<2L", self.client_key, self.server_key) 
        if self.flags & 0x04:
            message_buffer += pack("<L", self.cookie)
        return message_buffer

class BnlsServerHashDataPacket(BnlsServerPacket):
    def __init__(self, data_hash, cookie=0):
        self.id = BnlsPacketIds.id_hashdata
        self.data_hash = data_hash
        self.cookie = cookie

class BnlsClientLogonChallengePacket(BnlsClientPacket):
    def __init__(self, account_name, password):
        self.id = BnlsPacketIds.id_logonchallenge
        self.account_name = account_name + "\0"
        self.password = password + "\0"

    def get_message_buffer(self):
        return self.account_name + self.password

class BnlsServerLogonChallengePacket(BnlsServerPacket):
    def __init__(self, client_key):
        self.id = BnlsPacketIds.id_logonchallenge
        self.client_key = client_key

class BnlsClientLogonProofPacket(BnlsClientPacket):
    def __init__(self, salt, server_key):
        assert len(salt) == 32
        assert len(server_key) == 32
        self.id = BnlsPacketIds.id_logonproof
        self.salt = salt
        self.server_key = server_key

    def get_message_buffer(self):
        #return self.salt + self.server_key
        combined = self.salt + self.server_key
        buff = ""
        for i in range(0, len(combined), 4):
            uint32 = unpack(">L", combined[i:i+4])[0]
            print uint32
            buff += pack(">L", uint32)
        return buff


class BnlsServerLogonProofPacket(BnlsServerPacket):
    def __init__(self, client_password_proof):
        self.id = BnlsPacketIds.id_logonproof
        self.client_password_proof = client_password_proof

class BnlsProtocol(Protocol):
    def connectionMade(self):
        self.log("Connected")
        self.service.bnls_connected(self)
        self.packet_extractor = BnlsPacketExtractor(self)

    def connectionLost(self, reason):
        self.log("Disconnected")
        self.service.bnls_disconnected(reason.getErrorMessage())

    def dataReceived(self, data):
        self.log("Received data: {}".format(to_hex(data)))
        self.receive_packet(data)

    def send_packet(self, packet):
        self.log("Sending packet: {}".format(BnlsPacketNames.get(packet.id)))
        self.log(to_hex(packet.to_buffer()))
        self.transport.write(packet.to_buffer())

    def receive_packet(self, data):
        received_packets = self.packet_extractor.extract(data)
        if len(received_packets) == 0:
            self.log("Did not receive familiar BNLS packet")
            return
        for received_packet in received_packets:
            packet_id = received_packet.id
            if packet_id not in self.service_receive_functions:
                self.log("No handler for received packet with id: {:02x}" \
                         .format(packet_id))
                return
        self.service_receive_functions[packet_id](received_packet)

    def log(self, msg, prepend="BnlsProtocol"):
        self.service.log("{}".format(msg), prepend=prepend)

class BnlsProtocolFactory(ClientFactory):
    def __init__(self, bn_service):
        self.service = bn_service
        self.service_receive_functions = {
            BnlsPacketIds.id_authorize : self.service.got_bnls_authorize,
            BnlsPacketIds.id_authorizeproof : \
                    self.service.got_bnls_authorizeproof,
            BnlsPacketIds.id_requestversionbyte : \
                    self.service.got_bnls_requestversionbyte,
            BnlsPacketIds.id_versioncheck : self.service.got_bnls_versioncheck,
            BnlsPacketIds.id_cdkey : self.service.got_bnls_cdkey,
            BnlsPacketIds.id_hashdata : self.service.got_bnls_hashdata,
            BnlsPacketIds.id_cdkey_ex : self.service.got_bnls_cdkey_ex,
            BnlsPacketIds.id_logonchallenge : \
                    self.service.got_bnls_logonchallenge,
            BnlsPacketIds.id_logonproof: \
                    self.service.got_bnls_logonproof,
            }

    def buildProtocol(self, addr):
        bnls_protocol = BnlsProtocol()
        bnls_protocol.factory = self
        bnls_protocol.service = self.service
        bnls_protocol.service_receive_functions = self.service_receive_functions
        return bnls_protocol

class BnlsPacketExtractor(object):
    def __init__(self, packet_user=None):
        self.packet_user = packet_user

    def extract(self, data):
        if len(data) < 3:
            self.log("Data too short to be valid BNLS packet")
            return []
        self.log("Parsing packet data...")
        packets = []
        data_buffer = DataBuffer(data)
        while(data_buffer.unprocessed_bytes() >= 3):
            packet_length = data_buffer.extract_word()
            packet_id = data_buffer.extract_byte()
            if packet_id == BnlsPacketIds.id_authorize:
                server_code = data_buffer.extract_dword()
                packets.append(BnlsServerAuthorizePacket(server_code))
            elif packet_id == BnlsPacketIds.id_authorizeproof:
                status_code = data_buffer.extract_dword
                packets.append(BnlsServerAuthorizeProofPacket(status_code))
            elif packet_id == BnlsPacketIds.id_requestversionbyte:
                product_id = data_buffer.extract_dword()
                version_byte = 0
                if product_id != 0:
                    version_byte = data_buffer.extract_dword()
                packets.append(
                        BnlsServerRequestVersionBytePacket(
                            product_id,
                            version_byte))
            elif packet_id == BnlsPacketIds.id_versioncheck:
                success = data_buffer.extract_dword()
                if success == 0:
                    packets.append(BnlsServerVersionCheckPacket(success))
                    continue
                exe_version = data_buffer.extract_dword()
                exe_checksum = data_buffer.extract_dword()
                stat_string = data_buffer.extract_cstring()
                packets.append(
                        BnlsServerVersionCheckPacket(
                            success,
                            exe_version,
                            exe_checksum,
                            stat_string))
            elif packet_id == BnlsPacketIds.id_cdkey:
                success = data_buffer.extract_dword()
                if success == 0:
                    packets.append(BnlsServerCdKeyPacket(success))
                    continue
                client_token = data_buffer.extract_dword()
                cdkey_length = data_buffer.extract_dword()
                cdkey_product_value = data_buffer.extract_dword()
                cdkey_public_value = data_buffer.extract_dword()
                unknown = data_buffer.extract_dword()
                cdkey_hash = data_buffer.extract_dword_tuple(5) 
                packets.append(
                        BnlsServerCdKeyPacket(
                            success,
                            client_token,
                            cdkey_length,
                            cdkey_product_value,
                            cdkey_public_value,
                            unknown,
                            cdkey_hash))
            elif packet_id == BnlsPacketIds.id_hashdata:
                hash_data = data_buffer.extract_dword_tuple(5) 
                if packet_length == 27:
                    cookie = data_buffer.extract_dword()
                    packets.append(BnlsServerHashDataPacket(hash_data, cookie))
                    continue
                packets.append(BnlsServerHashDataPacket(hash_data))
            elif packet_id == BnlsPacketIds.id_cdkey_ex:
                cookie = data_buffer.extract_dword()
                requested = data_buffer.extract_byte()
                encrypted = data_buffer.extract_byte()
                if encrypted != requested:
                    self.log(
                    "Error: {} CD Keys requested, but only {} encrypted" \
                    .format(requested, encrypted))
                    continue
                bit_mask = data_buffer.extract_dword()
                client_session_key = data_buffer.extract_dword()
                cdkey_data = data_buffer.extract_dword_tuple(9)
                packets.append(
                        BnlsServerCdKeyExPacket(cookie,
                                                requested,
                                                encrypted,
                                                bit_mask,
                                                client_session_key,
                                                cdkey_data))
            elif packet_id == BnlsPacketIds.id_logonchallenge:
                #client_key = data_buffer.extract_bytes(32)
                client_key = data_buffer.extract_dword_tuple_bigendian(8)
                packets.append(BnlsServerLogonChallengePacket(client_key))
            elif packet_id == BnlsPacketIds.id_logonproof:
                #client_password_proof = data_buffer.extract_bytes(20)
                client_password_proof = data_buffer.extract_dword_tuple_bigendian(5)
                packets.append(
                        BnlsServerLogonProofPacket(client_password_proof))
            else:
                self.log("Unrecognized packet ID: {:02x}".format(packet_id))

        self.log("Extracted %d BNLS packet(s)" % len(packets))
        return packets

    def log(self, msg, prepend="BnlsPacketExtractor"):
        if self.packet_user is None:
            return
        self.packet_user.log(msg, prepend=prepend)
