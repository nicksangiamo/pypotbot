from twisted.internet.protocol import Protocol, ClientFactory
from struct import pack, unpack
from abc import ABCMeta, abstractmethod
from bnutil import *
import random

byte_size = 1
word_size = 2
dword_size = 4

class BncsPacketIds(object):
    id_null = 0x00
    id_enterchat = 0x0a
    id_getchannellist = 0X0b
    id_joinchannel = 0X0c
    id_chatcommand = 0X0e
    id_chatevent = 0X0f
    id_flooddetected = 0x13
    id_messagebox = 0x19
    id_ping = 0x25
    id_logonresponse2 = 0x3a
    id_authinfo = 0x50
    id_authcheck = 0x51
    id_authaccountlogon = 0x53
    id_authaccountlogonproof = 0x54
    id_friendslist = 0x65
    id_readuserdata = 0x26

class BncsPacketNames(object):
    names = {
            BncsPacketIds.id_null : "SID_NULL",
            BncsPacketIds.id_enterchat : "SID_ENTERCHAT",
            BncsPacketIds.id_getchannellist : "SID_GETCHANNELLIST",
            BncsPacketIds.id_joinchannel : "SID_JOINCHANNEL",
            BncsPacketIds.id_chatcommand : "SID_CHATCOMMAND",
            BncsPacketIds.id_chatevent : "SID_CHATEVENT",
            BncsPacketIds.id_ping : "SID_PING",
            BncsPacketIds.id_logonresponse2 : "SID_LOGONRESPONSE2",
            BncsPacketIds.id_authinfo : "SID_AUTH_INFO",
            BncsPacketIds.id_authcheck : "SID_AUTH_CHECK",
            BncsPacketIds.id_authaccountlogon : "SID_AUTH_ACCOUNTLOGON",
            BncsPacketIds.id_authaccountlogonproof : \
                    "SID_AUTH_ACCOUNTLOGONPROOF",
            BncsPacketIds.id_readuserdata : "SID_READUSERDATA",
            BncsPacketIds.id_friendslist : "SID_FRIENDSLIST",
            BncsPacketIds.id_null : "SID_NULL",
    }
    
    @staticmethod
    def get(packet_id):
        if packet_id in BncsPacketNames.names:
            return BncsPacketNames.names[packet_id] 
        else:
            return "BNCS_UNKNOWN_PACKET"

class BncsEventIds(object):
    id_showuser = 0x01
    id_join = 0x02
    id_leave = 0x03
    id_whisper = 0x04
    id_talk = 0x05
    id_broadcast = 0x06
    id_channel = 0x07
    id_userflags = 0x09
    id_whispersent = 0x0a
    id_channelfull = 0x0d
    id_channeldoesnotexist = 0x0e
    id_channelrestricted = 0x0f
    id_info = 0x12
    id_error = 0x13
    id_ignore = 0x15
    id_accept = 0x16
    id_emote = 0x17

class BncsEventNames(object):
    names = {
            BncsEventIds.id_showuser : "EID_SHOWUSER",
            BncsEventIds.id_join : "EID_JOIN",
            BncsEventIds.id_leave : "EID_LEAVE",
            BncsEventIds.id_whisper : "EID_WHISPER",
            BncsEventIds.id_talk : "EID_TALK",
            BncsEventIds.id_broadcast : "EID_BROADCAST",
            BncsEventIds.id_channel : "EID_CHANNEL",
            BncsEventIds.id_userflags : "EID_USERFLAGS",
            BncsEventIds.id_whispersent : "EID_WHISPERSENT",
            BncsEventIds.id_channelfull : "EID_CHANNELFULL",
            BncsEventIds.id_channeldoesnotexist : "EID_CHANNELDOESNOTEXIST",
            BncsEventIds.id_channelrestricted : "EID_RESTRICTED",
            BncsEventIds.id_info : "EID_INFO",
            BncsEventIds.id_error : "EID_ERROR",
            BncsEventIds.id_ignore : "EID_IGNORE",
            BncsEventIds.id_accept : "EID_ACCEPT",
            BncsEventIds.id_emote : "EID_EMOTE",
    }
    
    @staticmethod
    def get(event_id):
        if event_id in BncsEventNames.names:
            return BncsEventNames.names[event_id] 
        else:
            return "BNCS_UNKNOWN_EVENT"


#this is incomplete, and the non-starcraft ones are guesses
class BncsProductIds(object):
    id_starcraft = 0x53544152 #ascii STAR
    id_starcraft_bw = 0x53455850 #ascii SEXP
    id_warcraft_iii = 0x57415233 #ascii WAR3
    id_warcraft_iii_ft = 0x57335850 #ascii W3XP

#currently only contains profile and system keys
class BncsUserDataKeys:
    sex = "profile\\sex" 
    age = "proflie\\age"
    location = "profile\\location"
    account_created = "System\\Account Created"
    auth_level = "System\\AuthLevel"
    flags = "System\\Flags"
    friends = "System\\Friends"
    icon = "System\\Icon"
    ip = "System\\IP"
    last_logoff = "System\\Last Logoff"
    last_logon = "System\\Last Logon"
    league = "System\\League"
    mft_bytes = "System\\MFT Bytes"
    mft_last_write = "System\\MFT Last Write"
    mft_time_logged = "System\\MFT Time Logged"
    port = "System\\Port"
    time_logged = "System\\Time Logged"
    username = "System\\ Username"

class BncsClientPacket(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_message_buffer():
        pass

    def to_buffer(self):
        self.message_buffer = self.get_message_buffer()
        self.length = 4 + len(self.message_buffer)
        return pack("<BBH", 0xff, self.id, self.length) + self.message_buffer

class BncsServerPacket(object):
    __metaclass__ = ABCMeta
    
class BncsClientAuthInfoPacket(BncsClientPacket):
    def __init__(self, product_id, version_byte):
        self.id = BncsPacketIds.id_authinfo
        self.protocol_id = 0
        self.platform_id = self.get_platform_id()
        self.product_id = product_id
        self.version_byte = version_byte
        self.product_language = 0
        self.local_ip = 0
        self.timezone_bias = self.get_timezone_bias()
        self.locale_id = 0
        self.language_id = self.locale_id #Could these be different
        self.country_abbreviation = self.get_country_abbreviation()
        self.country = self.get_country()
    
    #TODO: implement this correctly
    def get_platform_id(self):
        return 0x49583836 #ascii IX86

    #TODO: implement this correctly
    def get_timezone_bias(self):
        return 4*60

    #TODO: implement this correctly
    def get_locale_id(self):
        return 0x09040000

    #TODO: implement this correctly
    def get_country_abbreviation(self):
        return "USA" + "\0"

    #TODO: implement this correctly
    def get_country(self):
        return "United States" + "\0"

    def get_message_buffer(self):
        return pack(
                "<9L",
                self.protocol_id,
                self.platform_id,
                self.product_id,
                self.version_byte,
                self.product_language,
                self.local_ip,
                self.timezone_bias,
                self.locale_id,
                self.language_id) + self.country_abbreviation + self.country

class BncsServerAuthInfoPacket(BncsServerPacket):
    def __init__(
            self,
            logon_type,
            server_token,
            udp_value,
            mpq_filetime,
            ix86ver_filename,
            valuestring):
        self.id = BncsPacketIds.id_authinfo
        self.logon_type = logon_type
        self.server_token = server_token
        self.udp_value = udp_value
        self.mpq_filetime = mpq_filetime
        self.ix86ver_filename = ix86ver_filename
        self.valuestring = valuestring

class BncsClientNullPacket(BncsClientPacket):
    def __init__(self):
        self.id = BncsPacketIds.id_null

    def get_message_buffer(self):
        return ""

class BncsClientPingPacket(BncsClientPacket):
    def __init__(self, ping_value):
        self.id = BncsPacketIds.id_ping
        self.ping_value = ping_value

    def get_message_buffer(self):
        return pack("<L", self.ping_value)

class BncsServerPingPacket(BncsServerPacket):
    def __init__(self, ping_value):
        self.id = BncsPacketIds.id_ping
        self.ping_value = ping_value

class BncsClientAuthCheckPacket(BncsClientPacket):
    def __init__(
            self,
            client_token,
            exe_version,
            exe_hash,
            num_cdkeys,
            spawn_cdkeys,
            cdkey_infos,
            exe_info,
            cdkey_owner):
        self.id = BncsPacketIds.id_authcheck
        self.client_token = client_token
        self.exe_version = exe_version
        self.exe_hash = exe_hash
        self.num_cdkeys = num_cdkeys
        self.spawn_cdkeys = spawn_cdkeys
        self.cdkey_infos = cdkey_infos
        self.exe_info = exe_info + "\0"
        self.cdkey_owner = cdkey_owner + "\0"

    def get_message_buffer(self):
        message_buffer = pack(
                "<5L",
                self.client_token,
                self.exe_version,
                self.exe_hash,
                self.num_cdkeys,
                self.spawn_cdkeys)
        for cdkey_info in self.cdkey_infos:
            message_buffer += cdkey_info.to_buffer()
        message_buffer += self.exe_info + self.cdkey_owner
        return message_buffer

class BncsServerAuthCheckPacket(BncsServerPacket):
    def __init__(self, result, additional_info):
        self.id = BncsPacketIds.id_authcheck
        self.result = result
        self.additional_info = additional_info

class BncsClientLogonResponse2Packet(BncsClientPacket):
    def __init__(self, client_token, server_token, password_hash, username):
        self.id = BncsPacketIds.id_logonresponse2
        self.client_token = client_token
        self.server_token = server_token
        self.password_hash = password_hash
        self.username = username + "\0"

    def get_message_buffer(self):
        return pack(
                "<7L",
                self.client_token,
                self.server_token,
                *self.password_hash) + self.username

class BncsServerNullPacket(BncsServerPacket):
    def __init__(self):
        self.id = BncsPacketIds.id_null

class BncsServerLogonResponse2Packet(BncsServerPacket):
    def __init__(self, status, additional_info=""):
        self.id = BncsPacketIds.id_logonresponse2
        self.status = status
        self.additional_info = additional_info

class BncsClientEnterChatPacket(BncsClientPacket):
    def __init__(self, username="", statstring=""):
        self.id = BncsPacketIds.id_enterchat
        self.username = username + "\0"
        self.statstring = statstring + "\0"

    def get_message_buffer(self):
        return self.username + self.statstring

class BncsServerEnterChatPacket(BncsServerPacket):
    def __init__(self, unique_name, statstring, account_name):
        self.id = BncsPacketIds.id_enterchat
        self.unique_name = unique_name
        self.statstring = statstring
        self.account_name = account_name

class BncsClientGetChannelListPacket(BncsClientPacket):
    def __init__(self, product_id):
        self.id = BncsPacketIds.id_getchannellist
        self.product_id = product_id

    def get_message_buffer(self):
        return pack("<L", self.product_id)

class BncsServerGetChannelListPacket(BncsServerPacket):
    def __init__(self, channel_names):
        self.id = BncsPacketIds.id_getchannellist
        self.channel_names = channel_names

class BncsClientJoinChannelPacket(BncsClientPacket):
    no_create_join = 0x00
    first_join = 0x01
    forced_join = 0x02
    d2_first_join = 0x05

    def __init__(self, flags, channel):
        self.id = BncsPacketIds.id_joinchannel
        self.flags = flags
        self.channel = channel + "\0"

    def get_message_buffer(self):
        return pack("<L", self.flags) + self.channel

class BncsServerChatEventPacket(BncsServerPacket):
    def __init__(
            self,
            event_id,
            flags,
            ping,
            ip_address,
            account_number,
            registration_authority,
            username,
            text):
        self.id = BncsPacketIds.id_chatevent
        self.event_id = event_id
        self.flags = flags
        self.ping = ping
        self.ip_address = ip_address
        self.account_number = account_number
        self.registration_authority = registration_authority
        self.username = username
        self.text = text

class BncsClientChatCommandPacket(BncsClientPacket):
    def __init__(self, text):
        self.id = BncsPacketIds.id_chatcommand
        self.text = text

    def get_message_buffer(self):
        return self.text + "\0"

class BncsServerMessageBoxPacket(BncsServerPacket):
    def __init__(self, style, text, caption):
        self.id = BncsPacketIds.id_messagebox
        self.style = style
        self.text = text
        self.caption = caption

class BncsServerFloodDetectedPacket(BncsServerPacket):
    def __init__(self):
        self.id = BncsPacketIds.id_flooddetected

class BncsClientAuthAccountLogonPacket(BncsClientPacket):
    def __init__(self, client_key, username):
        self.id = BncsPacketIds.id_authaccountlogon
        self.client_key = client_key
        self.username = username + "\0"

    def get_message_buffer(self):
        return self.client_key + self.username

class BncsServerAuthAccountLogonPacket(BncsServerPacket):
    def __init__(self, status, salt, server_key):
        self.id = BncsPacketIds.id_authaccountlogon
        self.status = status
        self.salt = salt
        self.server_key = server_key

class BncsClientAuthAccountLogonProofPacket(BncsClientPacket):
    def __init__(self, client_password_proof):
        self.id = BncsPacketIds.id_authaccountlogonproof
        self.client_password_proof = client_password_proof

    def get_message_buffer(self):
        return self.client_password_proof

class BncsServerAuthAccountLogonProofPacket(BncsServerPacket):
    def __init__(self, status, server_password_proof, additional_info):
        self.id = BncsPacketIds.id_authaccountlogonproof
        self.status = status
        self.server_password_proof = server_password_proof
        self.additional_info = additional_info

class BncsClientReadUserDataPacket(BncsClientPacket):
    def __init__(self, account, keys):
        self.id = BncsPacketIds.id_readuserdata
        self.num_accounts = 1
        self.num_keys = 4
        self.request_id = random.randrange(2**32)
        self.account = account + "\0"
        self.keys = keys

    def get_message_buffer(self):
        buff = pack("<LLL",
                     self.num_accounts,
                     self.num_keys,
                     self.request_id)
        buff += self.account
        for key in self.keys:
            buff += key + "\0"
        return buff

class BncsServerReadUserDataPacket(BncsServerPacket):
    def __init__(self, num_accounts, num_keys, request_id, key_values):
        self.id = BncsPacketIds.id_readuserdata
        self.num_accounts = num_accounts
        self.num_keys = num_keys
        self.request_id = request_id
        self.key_values = key_values

class BncsClientFriendsListPacket(BncsClientPacket):
    def __init__(self):
        self.id = BncsPacketIds.id_friendslist

    def get_message_buffer(self):
        return ""

class BncsProtocol(Protocol):
    def connectionMade(self):
        self.log("Connected")
        self.service.bncs_connected(self)
        self.packet_extractor = BncsPacketExtractor(self)

    def connectionLost(self, reason):
        self.log("Disconnected")
        self.service.bncs_disconnected(reason.getErrorMessage())

    def dataReceived(self, data):
        self.log("Received data: %s" % to_hex(data))
        self.receive_packets(data)

    def send_game_protocol_byte(self):
        self.log("Sending game protocol byte")
        self.transport.write(pack("B", 0x01))

    def send_packet(self, packet):
        self.log("Sending packet: %s" % BncsPacketNames.get(packet.id))
        self.log(to_hex(packet.to_buffer()))
        self.transport.write(packet.to_buffer())

    def receive_packets(self, data):
        received_packets = self.packet_extractor.extract(data)
        if len(received_packets) == 0:
            self.log("Did not receive familiar BNCS packet")
            return
        for received_packet in received_packets:
            packet_id = received_packet.id
            if packet_id not in self.service_receive_functions:
                self.log("No handler for received packet with id: %x" % packet_id)
                return
            self.service_receive_functions[packet_id](received_packet)

    def log(self, msg, prepend="BncsProtocol"):
        self.service.log("%s" % msg, prepend=prepend)

class BncsProtocolFactory(ClientFactory):
    def __init__(self, bn_session):
        self.service = bn_session
        self.service_receive_fucntions = {
            BncsPacketIds.id_authinfo : self.service.got_sid_authinfo,
            BncsPacketIds.id_authcheck : self.service.got_sid_authcheck,
            BncsPacketIds.id_ping : self.service.got_sid_ping,
            BncsPacketIds.id_logonresponse2 : \
                    self.service.got_sid_logonresponse2,
            BncsPacketIds.id_chatevent : self.service.got_sid_chatevent,
            BncsPacketIds.id_chatcommand : self.service.got_sid_chatcommand,
            BncsPacketIds.id_getchannellist : self.service.got_sid_channellist,
            BncsPacketIds.id_joinchannel : self.service.got_sid_joinchannel,
            BncsPacketIds.id_logonresponse2 : \
                    self.service.got_sid_logonresponse2,
            BncsPacketIds.id_enterchat : self.service.got_sid_enterchat,
            BncsPacketIds.id_getchannellist : \
                    self.service.got_sid_getchannellist,
            BncsPacketIds.id_authaccountlogon : \
                    self.service.got_sid_authaccountlogon,
            BncsPacketIds.id_authaccountlogonproof : \
                    self.service.got_sid_authaccountlogonproof,
            BncsPacketIds.id_readuserdata : \
                    self.service.got_sid_readuserdata,
            BncsPacketIds.id_null: self.service.got_sid_null,
        }

    def buildProtocol(self, addr):
        bncs_protocol = BncsProtocol()
        bncs_protocol.factory = self
        bncs_protocol.service = self.service
        bncs_protocol.service_receive_functions = self.service_receive_fucntions
        return bncs_protocol

class BncsPacketExtractor(object):
    def __init__(self, packet_user=None):
        self.packet_user = packet_user

    def extract(self, data):
        if len(data) < 4:
            self.log("Data too short to be valid BNCS packet")
            return []
        self.log("Parsing packet data...")
        packets = []
        data_buffer = DataBuffer(data)
        while(data_buffer.unprocessed_bytes() >= 4):
            ff_byte = data_buffer.extract_byte()
            if ff_byte != 0xff:
                break

            packet_id = data_buffer.extract_byte()
            packet_length = data_buffer.extract_word()

            if packet_id == BncsPacketIds.id_authinfo:
                logon_type = data_buffer.extract_dword()
                server_token = data_buffer.extract_dword()
                udp_value = data_buffer.extract_dword()
                mpq_filetime = data_buffer.extract_bytes(8)
                ix86ver_filename = data_buffer.extract_cstring()
                valuestring = data_buffer.extract_cstring()
                packets.append(BncsServerAuthInfoPacket(
                    logon_type,
                    server_token,
                    udp_value,
                    mpq_filetime,
                    ix86ver_filename,
                    valuestring))
            elif packet_id == BncsPacketIds.id_ping:
                ping_value = data_buffer.extract_dword()
                packets.append(BncsServerPingPacket(ping_value))
            elif packet_id == BncsPacketIds.id_authcheck:
                result = data_buffer.extract_dword()
                additional_info = data_buffer.extract_cstring()
                packets.append(BncsServerAuthCheckPacket(result, additional_info))
            elif packet_id == BncsPacketIds.id_logonresponse2:
                status = data_buffer.extract_dword()
                if status == 0x06:
                    packets.append(BncsServerLogonResponse2Packet(status, additional_info))
                    continue
                packets.append(BncsServerLogonResponse2Packet(status))
            elif packet_id == BncsPacketIds.id_enterchat:
                values = data_buffer.extract_bytes(packet_length-4).split("\0", 3)
                unique_name = values[0] if len(values) > 0 else ""
                statstring = values[1] if len(values) > 1 else ""
                account_name = values[2] if len(values) > 2 else ""
                packets.append(BncsServerEnterChatPacket(
                        unique_name,
                        statstring,
                        account_name))
            elif packet_id == BncsPacketIds.id_getchannellist:
                temp = data_buffer.extract_bytes(packet_length-4)
                channel_names = temp.split("\0")[:-2]
                packets.append(BncsServerGetChannelListPacket(channel_names))
            elif packet_id == BncsPacketIds.id_chatevent:
                event_id = data_buffer.extract_dword()
                flags = data_buffer.extract_dword()
                ping = data_buffer.extract_dword()
                ip_address = data_buffer.extract_dword()
                account_number = data_buffer.extract_dword()
                registration_authority = data_buffer.extract_dword()
                username = data_buffer.extract_cstring()
                text = data_buffer.extract_cstring()
                packets.append(BncsServerChatEventPacket(
                        event_id,
                        flags,
                        ping,
                        ip_address,
                        account_number,
                        registration_authority,
                        username,
                        text))
            elif packet_id == BncsPacketIds.id_messagebox:
                style = data_buffer.extract_dword()
                text = data_buffer.extract_cstring()
                caption = data_buffer.extract_cstring()
                packets.append(BncsServerMessageBoxPacket(style, text, caption))
            elif packet_id == BncsPacketIds.id_flooddetected:
                packets.append(BncsServerFloodDetectedPacket())
            elif packet_id == BncsPacketIds.id_authaccountlogon:
                status = data_buffer.extract_dword()
                salt = data_buffer.extract_bytes(32)
                server_key = data_buffer.extract_bytes(32)
                packets.append(BncsServerAuthAccountLogonPacket(
                    status,
                    salt,
                    server_key))
            elif packet_id == BncsPacketIds.id_authaccountlogonproof:
                status = data_buffer.extract_dword()
                server_password_proof = data_buffer.extract_bytes(20)
                additional_info = data_buffer.extract_cstring()
                packets.append(BncsServerAuthAccountLogonProofPacket(
                    status,
                    server_password_proof,
                    additional_info))
            elif packet_id == BncsPacketIds.id_readuserdata:
                num_accounts = data_buffer.extract_dword()
                num_keys = data_buffer.extract_dword()
                request_id = data_buffer.extract_dword()
                key_values = []
                while True:
                    key_value = data_buffer.extract_cstring()
                    if not key_value:
                        break
                    key_values.append(key_value)
                packets.append(BncsServerReadUserDataPacket(
                    num_accounts, num_keys,
                    request_id,
                    key_values))
            elif packet_id == BncsPacketIds.id_null:
                packets.append(BncsServerNullPacket())
            else:
                self.log("Unrecognized packet ID: {:02x}".format(packet_id))

        self.log("Extracted %d BNCS packet(s)" % len(packets))
        return packets

    def log(self, msg, prepend="BncsPacketExtractor"):
        if self.packet_user is None:
            return
        self.packet_user.log(msg, prepend=prepend)
