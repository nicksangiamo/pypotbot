from potbotextra import PotbotService, PotbotCommand
from time import time
from datetime import datetime

class DefinitionService(PotbotService):
    def __init__(self, potbot):
        super(self.__class__, self).__init__(potbot)
        self.dbpool = self.potbot.dbpool
        self.potbot.add_command("newdef", 55, self.command_newdef)
        self.potbot.add_command("def", 50, self.command_def)
        self.initialization_success()

    def commands_enabled(self):
        self.dbpool = self.potbot.dbpool
        return self.dbpool is not None

    def command_newdef(self, caller, args):
        if not self.commands_enabled():
            self.log_potbot("Got newdef command but commands aren't enabled")
            return
        arg_split = " ".join(args).split("|")
        if len(arg_split) < 2:
            self.log_potbot("No '|' provided with def command")
            return
        term = arg_split[0].lower().strip()
        meaning = arg_split[1].strip()
        if term == "":
            self.log_potbot("No term provided with def command")
            return
        if meaning == "":
            self.log_potbot("No meaning provided with def command")
            return
        def_entries_deferred = self.dbpool.runQuery(
                "select * from definition where term=%s", (term,))
        def_entries_deferred.addCallback(
                self.cmdnewdef_def_entries_received, caller, term, meaning)

    def cmdnewdef_def_entries_received(
            self, def_entries, definer, term, meaning):
        if len(def_entries) < 1:
            def_entries_deferred = self.dbpool.runQuery(
                    "insert into definition "
                    "(term,meaning,creator,creation_date) "
                    "values (%s,%s,%s,%s) "
                    "returning term,meaning,creator,creation_date",
                    (term,meaning,definer,datetime.now()))
            def_entries_deferred.addCallback(
                    self.cmdnewdef_inserted_def_entries)
            return
        update_deferred = self.dbpool.runQuery(
                "update definition "
                "set meaning=%s,last_modifier=%s,last_modified_date=%s "
                "where term=%s "
                "returning *",
                (meaning,definer,datetime.now(),term))
        update_deferred.addCallback(self.cmdnewdef_updated_def_entries)
    
    def cmdnewdef_inserted_def_entries(self, def_entries):
        if len(def_entries) < 1:
            return
        self.potbot.send_chat_message("Definition added.")

    def cmdnewdef_updated_def_entries(self, def_entries):
        if len(def_entries) < 1:
            return
        self.potbot.send_chat_message("Definition updated.")

    def command_def(self, caller, args):
        if not self.commands_enabled():
            self.log_potbot("Got def command but commands aren't enabled")
            return
        if len(args) < 1:
            self.log_potbot("Not enough args for def command")
            return
        term = args[0].lower().strip()
        def_entries_deferred = self.dbpool.runQuery(
                "select meaning from definition where term=%s", (term,))
        def_entries_deferred.addCallback(self.cmddef_def_entries_received, term)

    def cmddef_def_entries_received(self, def_entries, term):
        if len(def_entries) < 1:
            self.potbot.send_chat_message(
                    "No definition found for [{}].".format(term))
            return
        meaning = def_entries[0][0]
        self.potbot.send_chat_message("[{}]: {}".format(term, meaning))
