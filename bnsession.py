from bnls import *
from bncs import *
from bnutil import *
from twisted.internet import reactor
from time import sleep
import re
import ctypes

class BNCSUtil(object):
    def __init__(self, username, password):
        self.bncsutil = ctypes.cdll.LoadLibrary("libbncsutil.so")
        self.nls = None
        self.username = username
        self.password = password

    def init_nls(self):
        self.nls = self.bncsutil.nls_init(self.username, self.password)
        if not self.nls:
            raise RuntimeError("nls_init failed")

    def compute_client_key(self):
        assert self.nls
        auth_accountlogon_data_len = 33 + len(self.username)
        auth_accountlogon_data = ctypes.create_string_buffer(
                "\x00", 
                auth_accountlogon_data_len)
        num_bytes = self.bncsutil.nls_account_logon(
                self.nls, 
                auth_accountlogon_data,
                auth_accountlogon_data_len)
        if num_bytes == 0:
            raise RuntimeError("nls_account_logon failed")
        client_key = auth_accountlogon_data.value[0:32]
        return client_key

    def compute_client_password_proof(self, server_key, salt):
        assert self.nls
        proof_len = 20
        proof = ctypes.create_string_buffer("\x00" * proof_len)
        self.bncsutil.nls_get_M1(self.nls, proof, server_key, salt)
        return proof.value

class BnSession(object):
    def __init__(
            self, bn_application, username, password, product, cdkey,
            cdkey_owner, home_channel, bnls_address="phix.no-ip.org",
            bnls_port=9367, bncs_address="useast.battle.net", bncs_port=6112):
        self.bn_application = bn_application
        self.bnls_address = bnls_address
        self.bnls_port = bnls_port
        self.bncs_address = bncs_address
        self.bncs_port = bncs_port
        self.bnls_protocol = None
        self.bncs_protocol = None
        self.set_product_ids(product)
        self.username = username
        self.password = password
        self.cdkey = cdkey
        self.cdkey_infos = {}
        self.cdkey_owner = cdkey_owner
        self.cdkey_owners = {self.cdkey : self.cdkey_owner}
        self.home_channel = home_channel
        self.channels = []
        self.channel = None
        self.pre_channel_users = []
        self.version_byte = 0
        self.bnls_bot_id = "Stealth"
        self.status = {
            "logging_on":False,
            "bnls_connecting":False,
            "bnls_connected":False,
            "bnls_authorize":False,
            "bnls_authorizeproof":False,
            "bnls_requestversionbyte":False,
            "bnls_versioncheck":False,
            "bnls_hashdata":False,
            "bnls_cdkey":False,
            "bnls_cdkey_ex":False,
            "bnls_logonproof":False,
            "bnls_logonchallenge":False,
            "bncs_connecting":False,
            "bncs_connected":False,
            "bncs_authinfo":False,
            "bncs_authcheck":False,
            "bncs_authaccountlogon":False,
            "bncs_authaccountlogonproof":False,
            "hashing_password":False,
            "in_chat":False,
            "done_with_bnls":False,
            "done_with_bncs":False,
            }
        self.log("Starting BnSession")
        self.bnls_connector = None
        self.bncs_connector = None
        self.bnls_protocol_factory = BnlsProtocolFactory(self)
        self.bncs_protocol_factory = BncsProtocolFactory(self)
        self.bncsutil = BNCSUtil(self.username, self.password)
        self.bncs_keepalive_interval = 10
        self.bncs_friendslist_interval = 10

    def set_product_ids(self, product):
        if product.lower() == "star":
            self.bnls_product_id = BnlsProductIds.id_starcraft
            self.bncs_product_id = BncsProductIds.id_starcraft
        elif product.lower() == "sexp":
            self.bnls_product_id = BnlsProductIds.id_starcraft_bw
            self.bncs_product_id = BncsProductIds.id_starcraft_bw
        elif product.lower() == "war3":
            self.bnls_product_id = BnlsProductIds.id_warcraft_iii
            self.bncs_product_id = BncsProductIds.id_warcraft_iii
        elif product.lower() == "w3xp":
            self.bnls_product_id = BnlsProductIds.id_warcraft_iii_ft
            self.bncs_product_id = BncsProductIds.id_warcraft_iii_ft
        else:
            self.log("Invalid product: %s, defaulting to STAR" % product)
            self.bnls_product_id = BnlsProductIds.id_starcraft
            self.bncs_product_id = BncsProductIds.id_starcraft

    def set_state(self, state):
        if state in self.status and not self.status[state]:
            self.log("Entering state %s" % state)
            self.status[state] = True

    def clear_state(self, state):
        if state in self.status and self.status[state]:
            self.log("Leaving state %s" % state)
            self.status[state] = False

    def clear_state_type(self, state_type):
        for state in self.status:
            if state[0:len(state_type)] == state_type:
                self.clear_state(state)

    def in_state(self, state):
        if state in self.status:
            return self.status[state]
        return False

    def logon(self):
        self.set_state("logging_on")
        self.bnls_connect()
        self.bncs_connect()

    def bnls_connect(self):
        self.set_state("bnls_connecting")
        self.log("Connecting to BNLS")
        self.bnls_connector = reactor.connectTCP(self.bnls_address, self.bnls_port, self.bnls_protocol_factory)

    def bncs_connect(self):
        self.set_state("bncs_connecting")
        self.log("Connecting to BNCS")
        self.bncs_connector = reactor.connectTCP(self.bncs_address, self.bncs_port, self.bncs_protocol_factory)

    def bnls_connected(self, bnls_protocol):
        self.clear_state("bnls_connecting")
        self.set_state("bnls_connected")
        self.log("BNLS connected")
        self.bnls_protocol = bnls_protocol
        if self.in_state("logging_on"):
            #authorize_packet = BnlsClientAuthorizePacket(self.bnls_bot_id)
            #self.set_state("bnls_authorize")
            #self.bnls_protocol.send_packet(authorize_packet)
            requestversionbyte_packet = BnlsClientRequestVersionBytePacket(
                    self.bnls_product_id)
            self.set_state("bnls_requestversionbyte")
            self.bnls_protocol.send_packet(requestversionbyte_packet)

    def bnls_disconnected(self, error_msg):
        self.clear_state_type("bnls_connected")
        self.log(error_msg)
        self.log("BNLS disconnected")
        if not self.in_state("done_with_bnls"):
            self.bn_application.fatal_error("Lost BNLS connection")

    def bncs_disconnected(self, error_msg):
        self.clear_state_type("bncs_connected")
        self.log(error_msg)
        self.log("BNCS disconnected")
        if not self.in_state("done_with_bncs"):
            self.bn_application.fatal_error("Lost BNCS connection")

    def bncs_connected(self, bncs_protocol):
        self.clear_state("bncs_connecting")
        self.set_state("bncs_connected")
        self.log("BNCS connected")
        self.bncs_protocol = bncs_protocol
        if self.in_state("logging_on"):
            self.bncs_protocol.send_game_protocol_byte()

    def send_null_packet(self):
        if not self.in_state("bncs_connected"):
            return
        null_packet = BncsClientNullPacket()
        self.bncs_protocol.send_packet(null_packet)
        reactor.callLater(self.bncs_keepalive_interval, self.send_null_packet)

    def send_friendslist_packet(self):
        if not self.in_state("bncs_connected"):
            return
        friendslist_packet = BncsClientFriendsListPacket()
        self.bncs_protocol.send_packet(friendslist_packet)
        reactor.callLater(self.bncs_friendslist_interval,
                          self.send_friendslist_packet)

    def got_bnls_authorize(self, authorize_packet):
        self.log("Got BNLS_AUTHORIZE")
        if not self.in_state("bnls_authorize"):
            self.log("Ignoring it...")
            return
        self.clear_state("bnls_authorize")
        checksum = 0xbeef # checksum doesn't matter
        authorizeproof_packet = BnlsClientAuthorizeProofPacket(checksum)
        self.set_state("bnls_authorizeproof")
        self.bnls_protocol.send_packet(authorizeproof_packet)

    def got_bnls_authorizeproof(self, authorizeproof_packet):
        self.log("Got BNLS_AUTHORIZEPROOF")
        if not self.in_state("bnls_authorizeproof"):
            self.log("Ignoring it...")
            return
        status_code = authorizeproof_packet.status_code
        self.clear_state("bnls_authorizeproof")
        if status_code != 0:
            self.log("BNLS authorization failure for bot ID \"%s\"" % self.bnls_bot_id)
            self.log("Logged in as anonymous BNLS user")
        else:
            self.log("BNLS authorization success for bot ID \"%s\"" % self.bnls_bot_id)
        requestversionbyte_packet = BnlsClientRequestVersionBytePacket(self.bnls_product_id)
        self.set_state("bnls_requestversionbyte")
        self.bnls_protocol.send_packet(requestversionbyte_packet)

    def got_bnls_requestversionbyte(self, requestversionbyte_packet):
        self.log("Got BNLS_REQUESTVERSIONBYTE")
        if not self.in_state("bnls_requestversionbyte"):
            self.log("Ignoring it...")
            return
        self.clear_state("bnls_requestversionbyte")
        product_id = requestversionbyte_packet.product_id
        if product_id == 0:
            self.log("BNLS request version byte failure")
            return
        if product_id != self.bnls_product_id:
            self.log("Warning: got unexpected product id: %x" % (product_id))
            self.log("BNLS request version byte failure")
            return
        self.log("BNLS request version byte success")
        self.version_byte = requestversionbyte_packet.version_byte
        #TODO: attach this to a deferred or something for when bncs gets connected
        self.wait_bncs_connected()

    def got_bnls_versioncheck(self, versioncheck_packet):
        self.log("Got BNLS_VERSIONCHECK")
        if not self.in_state("bnls_versioncheck"):
            self.log("Ignoring it...")
            return
        self.clear_state("bnls_versioncheck")
        if versioncheck_packet.success == 0:
            self.log("BNLS version check failure")
            return
        self.log("BNLS version check success")
        self.exe_version = versioncheck_packet.exe_version
        self.exe_checksum = versioncheck_packet.exe_checksum
        self.version_check_stat_string = versioncheck_packet.stat_string

    def got_bnls_cdkey(self, cdkey_packet):
        self.log("Got BNLS_CDKEY")
        if not self.in_state("bnls_cdkey"):
            self.log("Ignoring it...")
            return
        self.clear_state("bnls_cdkey")
        self.client_token = cdkey_packet.client_token
        self.cdkey_infos[self.cdkey] = CdKeyInfo(
                cdkey_packet.cdkey_length,
                cdkey_packet.cdkey_product_value,
                cdkey_packet.cdkey_public_value,
                cdkey_packet.unknown,
                cdkey_packet.cdkey_hash)
        authcheck_packet = BncsClientAuthCheckPacket(
                self.client_token,
                self.exe_version,
                self.exe_checksum,
                1,
                0,
                [self.cdkey_infos[self.cdkey]],
                self.version_check_stat_string,
                self.cdkey_owners[self.cdkey])
        self.set_state("bncs_authcheck")
        self.bncs_protocol.send_packet(authcheck_packet)

    def got_bnls_hashdata(self, hashdata_packet):
        self.log("Got BNLS_HASHDATA")
        if not self.in_state("bnls_hashdata"):
            self.log("Ignoring it...")
            return
        self.clear_state("bnls_hashdata")
        if self.in_state("hashing_password"):
            self.clear_state("hashing_password")
            self.set_state("done_with_bnls")
            password_hash = hashdata_packet.data_hash
            logonresponse2_packet = BncsClientLogonResponse2Packet(
                    self.client_token,
                    self.server_token,
                    password_hash,
                    self.username)
            self.bncs_protocol.send_packet(logonresponse2_packet)
            self.log("Disconnecting BNLS")
            self.bnls_connector.disconnect()

    def got_bnls_cdkey_ex(self, cdkey_ex_packet):
        self.log("Got BNLS_CDKEY_EX")
        if not self.in_state("bnls_cdkey_ex"):
            self.log("Ignoring it...")
            return
        self.clear_state("bnls_cdkey_ex")
        self.client_token = self.client_token = cdkey_ex_packet.client_token
        cdkey_data = cdkey_ex_packet.cdkey_data
        self.cdkey_infos[self.cdkey] = CdKeyInfo(
                unpack("<L", cdkey_data[0])[0],
                unpack("<L", cdkey_data[1])[0],
                unpack("<L", cdkey_data[2])[0],
                unpack("<L", cdkey_data[3])[0],
                cdkey_data[4:])
        authcheck_packet = BncsClientAuthCheckPacket(
                self.client_token,
                self.exe_version,
                self.exe_checksum,
                1,
                0,
                [self.cdkey_infos[self.cdkey]],
                self.version_check_stat_string,
                self.cdkey_owners[self.cdkey])
        self.set_state("bncs_authcheck")
        self.bncs_protocol.send_packet(authcheck_packet)

    def got_bnls_logonchallenge(self, logonchallenge_packet):
        self.log("Got BNLS_LOGONCHALLENGE")
        if not self.in_state("bnls_logonchallenge"):
            self.log("Ignoring it...")
            return
        self.clear_state("bnls_logonchallenge")
        client_key = logonchallenge_packet.client_key
        authaccountlogon_packet = BncsClientAuthAccountLogonPacket(
                client_key,
                self.username)
        self.set_state("bncs_authaccountlogon")
        self.bncs_protocol.send_packet(authaccountlogon_packet)

    def got_bnls_logonproof(self, logonproof_packet):
        self.log("Got BNLS_LOGONPROOF")
        if not self.in_state("bnls_logonproof"):
            self.log("Ignoring it...")
            return
        self.clear_state("bnls_logonproof")
        client_password_proof = logonproof_packet.client_password_proof
        authaccountlogonproof_packet = BncsClientAuthAccountLogonProofPacket(
                client_password_proof)
        self.set_state("bncs_authaccountlogonproof")
        self.bncs_protocol.send_packet(authaccountlogonproof_packet)

    def got_sid_authinfo(self, authinfo_packet):
        self.log("Got SID_AUTH_INFO")
        if not self.in_state("bncs_authinfo"):
            self.log("Ignoring it...")
            return
        self.clear_state("bncs_authinfo")
        self.logon_type = authinfo_packet.logon_type
        self.server_token = authinfo_packet.server_token
        self.udp_value = authinfo_packet.udp_value
        self.ix86ver_filename = authinfo_packet.ix86ver_filename
        self.valuestring = authinfo_packet.valuestring
        version_dll_digit = self.extract_version_dll_digit(self.ix86ver_filename)
        versioncheck_packet = BnlsClientVersionCheckPacket(
                self.bnls_product_id,
                version_dll_digit,
                self.valuestring)
        self.set_state("bnls_versioncheck")
        self.bnls_protocol.send_packet(versioncheck_packet)
        #cdkey_packet = BnlsClientCdKeyPacket(self.server_token, self.cdkey)
        #self.set_state("bnls_cdkey")
        cdkey_ex_packet = BnlsClientCdKeyExPacket(self.server_token, self.cdkey)
        self.set_state("bnls_cdkey")
        self.bnls_protocol.send_packet(cdkey_ex_packet)

    def got_sid_ping(self, ping_packet):
        self.log("Got SID_PING")
        ping_value = ping_packet.ping_value
        ping_response_packet = BncsClientPingPacket(ping_value)
        self.bncs_protocol.send_packet(ping_response_packet)
    
    def got_sid_authcheck(self, authcheck_packet):
        self.log("Got SID_AUTH_CHECK")
        if not self.in_state("bncs_authcheck"):
            self.log("Ignoring it...")
            return
        self.clear_state("bncs_authcheck")
        authcheck_result = authcheck_packet.result
        additional_info = authcheck_packet.additional_info
        if authcheck_result != 0x000:
            self.log("BNCS auth check failure")
            if authcheck_result == 0x100:
                self.log("Reason: old game version: "
                         "patch mpq is %s" % additional_info)
            elif authcheck_result == 0x101:
                self.log("Reason: invalid version")
            elif authcheck_result == 0x102:
                self.log("Reason: game version must be downgraded: "
                         "patch mpq is %s" % additional_info)
            elif not authcheck_result & 0xf00:
                self.log("Reason: invalid version code supplied in "
                         "SID_AUTH_INFO: %x" % authcheck_result)
            elif authcheck_result in [0x200, 0x210]:
                self.log("Reason: invalid cd key")
            elif authcheck_result in [0x201, 0x211]:
                self.log("Reason: cd key currently in use by %s" % additional_info)
            elif authcheck_result in [0x202, 0x212]:
                self.log("Reason: cd key is banned")
            elif authcheck_result in [0x203, 0x213]:
                self.log("Reason: cd key is for the wrong product")
            return
        self.log("BNCS auth check success")
        if self.bncs_product_id == BncsProductIds.id_warcraft_iii:
            #logonchallenge_packet = BnlsClientLogonChallengePacket(
            #        self.username,
            #        self.password)
            #self.set_state("bnls_logonchallenge")
            #self.bnls_protocol.send_packet(logonchallenge_packet)
            self.bncsutil.init_nls()
            client_key = self.bncsutil.compute_client_key()
            authaccountlogon_packet = BncsClientAuthAccountLogonPacket(
                    client_key,
                    self.username)
            self.set_state("bncs_authaccountlogon")
            self.bncs_protocol.send_packet(authaccountlogon_packet)
        else:
            hashdata_packet = BnlsClientHashDataPacket(
                    len(self.password),
                    0x02, #double hash
                    self.password,
                    self.client_token,
                    self.server_token)
            self.set_state("bnls_hashdata")
            self.set_state("hashing_password")
            self.bnls_protocol.send_packet(hashdata_packet)


    def got_sid_null(self, null_packet):
        self.log("Got SID_NULL")
        return

    def got_sid_enterchat(self, enterchat_packet):
        self.log("Got SID_ENTERCHAT")
        unique_name = enterchat_packet.unique_name
        statstring = enterchat_packet.statstring
        account_name = enterchat_packet.account_name
        self.set_state("in_chat")
        self.bn_application.logged_on(self.username)
        if self.bncs_product_id ==  BncsProductIds.id_warcraft_iii:
            getchannellist_packet = BncsClientGetChannelListPacket(
                    self.bncs_product_id)
            joinchannel_packet = BncsClientJoinChannelPacket(
                    BncsClientJoinChannelPacket.no_create_join,
                    self.home_channel)
            readuserdata_packet = BncsClientReadUserDataPacket(
                    self.username,
                    [BncsUserDataKeys.account_created,
                     BncsUserDataKeys.last_logon,
                     BncsUserDataKeys.last_logoff,
                     BncsUserDataKeys.time_logged])
            chatcommand_packet = BncsClientChatCommandPacket("/whoami")
            self.bncs_protocol.send_packet(getchannellist_packet)
            self.bncs_protocol.send_packet(joinchannel_packet)
            self.bncs_protocol.send_packet(readuserdata_packet)
            self.bncs_protocol.send_packet(chatcommand_packet)
            ## TODO: might be a good idea to actually start sending this earlier
            #self.send_null_packet()

    def got_sid_getchannellist(self, getchannellist_packet):
        self.log("Got SID_GETCHANNELLIST")
        self.channels = getchannellist_packet.channel_names
        friendslist_packet = BncsClientFriendsListPacket()
        joinchannel_packet = BncsClientJoinChannelPacket(
                BncsClientJoinChannelPacket.d2_first_join,
                self.home_channel)
        joinchannel_packet2 = BncsClientJoinChannelPacket(
                BncsClientJoinChannelPacket.forced_join,
                self.home_channel)
        self.bncs_protocol.send_packet(friendslist_packet)
        self.bncs_protocol.send_packet(joinchannel_packet)
        self.bncs_protocol.send_packet(joinchannel_packet2)
        #reactor.callLater(1.1, self.send_friendslist_packet)
    
    def got_sid_chatevent(self, chatevent_packet):
        self.log("Got SID_CHATEVENT")
        event_id = chatevent_packet.event_id
        flags = chatevent_packet.flags
        ping = chatevent_packet.ping
        username = chatevent_packet.username
        text = chatevent_packet.text
        self.log("GOT %s" % BncsEventNames.get(event_id))
        if event_id == BncsEventIds.id_showuser:
            self.log("Channel user: %s" % username)
            self.add_channel_user(username)
            self.bn_application.got_showuser_event(username, flags, text, ping)
        elif event_id == BncsEventIds.id_join:
            self.log("User %s has joined the channel" % username)
            self.add_channel_user(username)
            self.log("Current channel users (%d): %s" % (self.channel.num_users(), ", ".join(self.channel.users)))
            self.bn_application.got_join_event(username, flags, text, ping)
        elif event_id == BncsEventIds.id_leave:
            self.log("User %s has left the channel" % username)
            self.remove_channel_user(username)
            self.log("Current channel users (%d): %s" % (self.channel.num_users(), ", ".join(self.channel.users)))
            self.bn_application.got_leave_event(username, flags, text, ping)
        elif event_id == BncsEventIds.id_whisper:
            self.log("Whisper from %s: %s" % (username, text))
            self.bn_application.got_whisper_event(username, flags, text, ping)
        elif event_id == BncsEventIds.id_talk:
            self.log("Talk from %s: %s" % (username, text))
            self.bn_application.got_talk_event(username, flags, text, ping)
        elif event_id == BncsEventIds.id_broadcast:
            self.log("Broadcast from %s: %s" % (username, text))
            self.bn_application.got_broadcast_event(username, flags, text, ping)
        elif event_id == BncsEventIds.id_channel:
            self.log("Joined channel %s" % text)
            self.channel = BnChannel(text, self.pre_channel_users)
            self.pre_channel_users = []
            self.bn_application.got_channel_event(username, flags, text, ping)
        elif event_id == BncsEventIds.id_userflags:
            self.log("Userflags update for %s: %s" % (username, text))
            self.bn_application.got_userflags_event(username, flags, text, ping)
        elif event_id == BncsEventIds.id_whispersent:
            self.log("Whisper to %s: %s" % (username, text))
            self.bn_application.got_whispersent_event(username, flags, text, ping)
        elif event_id == BncsEventIds.id_channeldoesnotexist:
            self.log("Channel does not exist: %s" % text) #TODO: does text actually have the channel name?
            self.bn_application.got_channeldoesnotexist_event(username, flags, text, ping)
        elif event_id == BncsEventIds.id_channelrestricted:
            self.log("Channel is restricted: %s" % text) #TODO: does text actually have the channel name?
            self.bn_application.got_channelrestricted_event(username, flags, text, ping)
        elif event_id == BncsEventIds.id_info:
            self.log("Info from battle.net: %s" % text)
            self.bn_application.got_info_event(username, flags, text, ping)
        elif event_id == BncsEventIds.id_error:
            self.log("Error: %s" % text)
            self.bn_application.got_error_event(username, flags, text, ping)
        elif event_id == BncsEventIds.id_emote:
            self.log("Emote from %s: %s" % (username, text))
            self.bn_application.got_emote_event(username, flags, text, ping)
        else:
            self.log("Unknown event id: %d" % event_id)
    
    def got_sid_channellist(self, channellist_packet):
        pass
    
    def got_sid_chatcommand(self, chatcommand_packet):
        pass
    
    def got_sid_joinchannel(self, joinchannel_packet):
        pass
    
    def got_sid_logonresponse2(self, logonresponse2_packet):
        self.log("Got SID_LOGONRESPONSE2")
        status = logonresponse2_packet.status
        if status != 0x00:
            self.log("BNCS logonresponse2 failure")
            if status == 0x01:
                self.log("Reason: account does not exist")
            elif status == 0x02:
                self.log("Reason: invalid password")
            elif status == 0x06:
                additional_info = logonresponse2_packet.additional_info
                self.log("Reason: account closed %s" % additional_info)
            else:
                self.log("Unknown reason code: 0x{:02X}".format(status))
            return
        self.log("BNCS logonresponse2 success")
        username = ""
        if self.bncs_product_id not in [BncsProductIds.id_warcraft_iii, BncsProductIds.id_warcraft_iii_ft]:
            username = self.username
        enterchat_packet = BncsClientEnterChatPacket(username)
        getchannellist_packet = BncsClientGetChannelListPacket(self.bncs_product_id)
        joinchannel_packet = BncsClientJoinChannelPacket(
                BncsClientJoinChannelPacket.forced_join,
                self.home_channel)
        self.bncs_protocol.send_packet(enterchat_packet)
        self.bncs_protocol.send_packet(getchannellist_packet)
        self.bncs_protocol.send_packet(joinchannel_packet)

    def got_sid_authaccountlogon(self, authaccountlogon_packet):
        self.log("Got SID_AUTH_ACCOUNTLOGON")
        if not self.in_state("bncs_authaccountlogon"):
            self.log("Ignoring it...")
            return
        self.clear_state("bncs_authaccountlogon")
        status = authaccountlogon_packet.status
        if status != 0x00:
            self.log("BNCS authaccountlogon failure")
            if status == 0x01:
                self.log("Reason: account does not exist")
            elif status == 0x05:
                self.log("Reason: account requires upgrade")
            else:
                self.log("Unknown reason code: 0x{:02X}".format(status))
            return
        salt = authaccountlogon_packet.salt
        server_key = authaccountlogon_packet.server_key
        client_password_proof = self.bncsutil.compute_client_password_proof(
                server_key,
                salt)
        authaccountlogonproof_packet = BncsClientAuthAccountLogonProofPacket(
                client_password_proof)
        self.set_state("bncs_authaccountlogonproof")
        self.bncs_protocol.send_packet(authaccountlogonproof_packet)
        #logonproof_packet = BnlsClientLogonProofPacket(salt, server_key)
        #self.set_state("bnls_logonproof")
        #self.bnls_protocol.send_packet(logonproof_packet)

    def got_sid_authaccountlogonproof(self, authaccountlogonproof_packet):
        self.log("Got SID_AUTH_ACCOUNTLOGONPROOF")
        if not self.in_state("bncs_authaccountlogonproof"):
            self.log("Ignoring it...")
            return
        self.clear_state("bncs_authaccountlogonproof")
        status = authaccountlogonproof_packet.status
        if status != 0x00:
            self.log("BNCS authaccountlogonproof failure")
            if status == 0x02:
                self.log("Reason: incorrect password")
            elif status == 0x06:
                self.log("Reason: account closed")
            elif status == 0x0e:
                self.log(
                        "Reason: An email address should be registered for "
                        "this account")
            elif status == 0x0f:
                self.log("Reason: custom error: {}".format(additional_info))
            else:
                self.log("Unknown reason code: 0x{:02X}".format(status))
            return
        enterchat_packet = BncsClientEnterChatPacket(self.username)
        #getchannellist_packet = BncsClientGetChannelListPacket(
        #        self.bncs_product_id)
        #joinchannel_packet = BncsClientJoinChannelPacket(
        #        BncsClientJoinChannelPacket.forced_join,
        #        self.home_channel)
        self.bncs_protocol.send_packet(enterchat_packet)
        #self.bncs_protocol.send_packet(getchannellist_packet)
        #self.bncs_protocol.send_packet(joinchannel_packet)

    def got_sid_readuserdata(self, readuserdata_packet):
        self.log("Got SID_READUSERDATA")

    def send_chat_message(self, message):
        message = message[:224]
        for i in range(0,20):
            message.replace(chr(i), "")
        chatcommand_packet = BncsClientChatCommandPacket(message)
        self.bncs_protocol.send_packet(chatcommand_packet)
    
    def add_channel_user(self, user):
        if self.channel is None:
            if user not in self.pre_channel_users:
                self.pre_channel_users.append(user)
            return
        self.channel.add_user(user)

    def remove_channel_user(self, user):
        if self.channel is None:
            if user in self.pre_channel_users:
                self.pre_channel_users.remove(user)
            return
        self.channel.remove_user(user)

    def extract_version_dll_digit(self, version_filename):
        digit_match = re.search(r"-(\d+)\.mpq", version_filename)
        try:
            digit = int(digit_match.group(1))
        except:
            return
        return digit
    
    def wait_bncs_connected(self):
        if not self.in_state("bncs_connected"):
            reactor.callLater(.5, self.wait_bncs_connected)
            return
        authinfo_packet = BncsClientAuthInfoPacket(self.bncs_product_id, self.version_byte)
        self.set_state("bncs_authinfo")
        self.bncs_protocol.send_packet(authinfo_packet)

    def log(self, msg, prepend="BnSession"):
        self.bn_application.log(msg, prepend=prepend)

