from abc import ABCMeta, abstractmethod
from struct import pack, unpack
from bnutil import *

#do i need to to prevent these from "polluting the global namespace"
byte_size = 1
word_size = 2
dword_size = 4

class BncsPacketIds(object):
    id_enterchat = 0x0a
    id_getchannellist = 0X0b
    id_joinchannel = 0X0c
    id_chatcommand = 0X0e
    id_chatevent = 0X0f
    id_ping = 0x25
    id_logonresponse2 = 0x3a
    id_authinfo = 0x50
    id_authcheck = 0x51

class BncsPacketNames(object):
    names = {
            BncsPacketIds.id_enterchat : "SID_ENTERCHAT",
            BncsPacketIds.id_getchannellist : "SID_GETCHANNELLIST",
            BncsPacketIds.id_joinchannel : "SID_JOINCHANNEL",
            BncsPacketIds.id_chatcommand : "SID_CHATCOMMAND",
            BncsPacketIds.id_chatevent : "SID_CHATEVENT",
            BncsPacketIds.id_ping : "SID_PING",
            BncsPacketIds.id_logonresponse2 : "SID_LOGONRESPONSE2",
            BncsPacketIds.id_authinfo : "SID_AUTH_INFO",
            BncsPacketIds.id_authcheck : "SID_AUTH_CHECK",
    }
    
    @staticmethod
    def get(packet_id):
        if packet_id in BncsPacketNames.names:
            return BncsPacketNames.names[packet_id] 
        else:
            return "BNCS_UNKNOWN_PACKET"

class BncsEventIds(object):
    id_showuser = 0x01
    id_join = 0x02
    id_leave = 0x03
    id_whisper = 0x04
    id_talk = 0x05
    id_broadcast = 0x06
    id_channel = 0x07
    id_userflags = 0x09
    id_whispersent = 0x0a
    id_channelfull = 0x0d
    id_channeldoesnotexist = 0x0e
    id_channelrestricted = 0x0f
    id_info = 0x12
    id_error = 0x13
    id_ignore = 0x15
    id_accept = 0x16
    id_emote = 0x17

class BncsEventNames(object):
    names = {
            BncsEventIds.id_showuser : "EID_SHOWUSER",
            BncsEventIds.id_join : "EID_JOIN",
            BncsEventIds.id_leave : "EID_LEAVE",
            BncsEventIds.id_whisper : "EID_WHISPER",
            BncsEventIds.id_talk : "EID_TALK",
            BncsEventIds.id_broadcast : "EID_BROADCAST",
            BncsEventIds.id_channel : "EID_CHANNEL",
            BncsEventIds.id_userflags : "EID_USERFLAGS",
            BncsEventIds.id_whispersent : "EID_WHISPERSENT",
            BncsEventIds.id_channelfull : "EID_CHANNELFULL",
            BncsEventIds.id_channeldoesnotexist : "EID_CHANNELDOESNOTEXIST",
            BncsEventIds.id_channelrestricted : "EID_RESTRICTED",
            BncsEventIds.id_info : "EID_INFO",
            BncsEventIds.id_error : "EID_ERROR",
            BncsEventIds.id_ignore : "EID_IGNORE",
            BncsEventIds.id_accept : "EID_ACCEPT",
            BncsEventIds.id_emote : "EID_EMOTE",
    }
    
    @staticmethod
    def get(event_id):
        if event_id in BncsEventNames.names:
            return BncsEventNames.names[event_id] 
        else:
            return "BNCS_UNKNOWN_EVENT"


#this is incomplete, and the non-starcraft ones are guesses
class BncsProductIds(object):
    id_starcraft = 0x53544152 #ascii STAR
    id_starcraft_bw = 0x53455850 #ascii SEXP
    id_warcraft_iii = 0x57415233 #ascii WAR3
    id_warcraft_iii_ft = 0x57335850 #ascii W3XP

class BncsClientPacket(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_message_buffer():
        pass

    def to_buffer(self):
        self.message_buffer = self.get_message_buffer()
        self.length = 4 + len(self.message_buffer)
        return pack("<BBH", 0xff, self.id, self.length) + self.message_buffer

class BncsServerPacket(object):
    __metaclass__ = ABCMeta
    
class BncsClientAuthInfoPacket(BncsClientPacket):
    def __init__(self, product_id, version_byte):
        self.id = BncsPacketIds.id_authinfo
        self.protocol_id = 0
        self.platform_id = self.get_platform_id()
        self.product_id = product_id
        self.version_byte = version_byte
        self.product_language = 0
        self.local_ip = 0
        self.timezone_bias = self.get_timezone_bias()
        self.locale_id = 0
        self.language_id = self.locale_id #Could these be different
        self.country_abbreviation = self.get_country_abbreviation()
        self.country = self.get_country()
    
    #TODO: implement this correctly
    def get_platform_id(self):
        return 0x49583836 #ascii IX86

    #TODO: implement this correctly
    def get_timezone_bias(self):
        return 4*60

    #TODO: implement this correctly
    def get_locale_id(self):
        return 0x09040000

    #TODO: implement this correctly
    def get_country_abbreviation(self):
        return "USA" + "\0"

    #TODO: implement this correctly
    def get_country(self):
        return "United States" + "\0"

    def get_message_buffer(self):
        print self.product_id
        print self.version_byte
        return pack(
                "<9L",
                self.protocol_id,
                self.platform_id,
                self.product_id,
                self.version_byte,
                self.product_language,
                self.local_ip,
                self.timezone_bias,
                self.locale_id,
                self.language_id) + self.country_abbreviation + self.country

class BncsServerAuthInfoPacket(BncsServerPacket):
    def __init__(
            self,
            logon_type,
            server_token,
            udp_value,
            mpq_filetime,
            ix86ver_filename,
            valuestring):
        self.id = BncsPacketIds.id_authinfo
        self.logon_type = logon_type
        self.server_token = server_token
        self.udp_value = udp_value
        self.mpq_filetime = mpq_filetime
        self.ix86ver_filename = ix86ver_filename
        self.valuestring = valuestring

class BncsClientPingPacket(BncsClientPacket):
    def __init__(self, ping_value):
        self.id = BncsPacketIds.id_ping
        self.ping_value = ping_value

    def get_message_buffer(self):
        return pack("<L", self.ping_value)

class BncsServerPingPacket(BncsServerPacket):
    def __init__(self, ping_value):
        self.id = BncsPacketIds.id_ping
        self.ping_value = ping_value

class BncsClientAuthCheckPacket(BncsClientPacket):
    def __init__(
            self,
            client_token,
            exe_version,
            exe_hash,
            num_cdkeys,
            spawn_cdkeys,
            cdkey_infos,
            exe_info,
            cdkey_owner):
        self.id = BncsPacketIds.id_authcheck
        self.client_token = client_token
        self.exe_version = exe_version
        self.exe_hash = exe_hash
        self.num_cdkeys = num_cdkeys
        self.spawn_cdkeys = spawn_cdkeys
        self.cdkey_infos = cdkey_infos
        self.exe_info = exe_info + "\0"
        self.cdkey_owner = cdkey_owner + "\0"

    def get_message_buffer(self):
        message_buffer = pack(
                "<5L",
                self.client_token,
                self.exe_version,
                self.exe_hash,
                self.num_cdkeys,
                self.spawn_cdkeys)
        for cdkey_info in self.cdkey_infos:
            message_buffer += cdkey_info.to_buffer()
        message_buffer += self.exe_info + self.cdkey_owner
        return message_buffer

class BncsServerAuthCheckPacket(BncsServerPacket):
    def __init__(self, result, additional_info):
        self.id = BncsPacketIds.id_authcheck
        self.result = result
        self.additional_info = additional_info

class BncsClientLogonResponse2Packet(BncsClientPacket):
    def __init__(self, client_token, server_token, password_hash, username):
        self.id = BncsPacketIds.id_logonresponse2
        self.client_token = client_token
        self.server_token = server_token
        self.password_hash = password_hash
        self.username = username + "\0"

    def get_message_buffer(self):
        return pack(
                "<7L",
                self.client_token,
                self.server_token,
                *self.password_hash) + self.username

class BncsServerLogonResponse2Packet(BncsServerPacket):
    def __init__(self, status, additional_info=""):
        self.id = BncsPacketIds.id_logonresponse2
        self.status = status
        self.additional_info = additional_info

class BncsClientEnterChatPacket(BncsClientPacket):
    def __init__(self, username="", statstring=""):
        self.id = BncsPacketIds.id_enterchat
        self.username = username + "\0"
        self.statstring = statstring + "\0"

    def get_message_buffer(self):
        return self.username + self.statstring

class BncsServerEnterChatPacket(BncsServerPacket):
    def __init__(self, unique_name, statstring, account_name):
        self.id = BncsPacketIds.id_enterchat
        self.unique_name = unique_name
        self.statstring = statstring
        self.account_name = account_name

class BncsClientGetChannelListPacket(BncsClientPacket):
    def __init__(self, product_id):
        self.id = BncsPacketIds.id_getchannellist
        self.product_id = product_id

    def get_message_buffer(self):
        return pack("<L", self.product_id)

class BncsServerGetChannelListPacket(BncsServerPacket):
    def __init__(self, channel_names):
        self.id = BncsPacketIds.id_getchannellist
        self.channel_names = channel_names

class BncsClientJoinChannelPacket(BncsClientPacket):
    no_create_join = 0x00
    first_join = 0x01
    forced_join = 0x02
    d2_first_join = 0x05

    def __init__(self, flags, channel):
        self.id = BncsPacketIds.id_joinchannel
        self.flags = flags
        self.channel = channel + "\0"

    def get_message_buffer(self):
        return pack("<L", self.flags) + self.channel

class BncsServerChatEventPacket(BncsServerPacket):
    def __init__(
            self,
            event_id,
            flags,
            ping,
            ip_address,
            account_number,
            registration_authority,
            username,
            text):
        self.id = BncsPacketIds.id_chatevent
        self.event_id = event_id
        self.flags = flags
        self.ping = ping
        self.ip_address = ip_address
        self.account_number = account_number
        self.registration_authority = registration_authority
        self.username = username
        self.text = text

class BncsClientChatCommandPacket(BncsClientPacket):
    def __init__(self, text):
        self.id = BncsPacketIds.id_chatcommand
        self.text = text

    def get_message_buffer(self):
        return self.text + "\0"
