import random
from potbotextra import PotbotService

RESPONSES_PATH = "./8ball.txt"

class Magic8BallService(PotbotService):
    def __init__(self, potbot):
        super(self.__class__, self).__init__(potbot)
        self.potbot.add_command("8ball", 50, self.command_8ball)
        self.potbot.add_command("ask8ball", 50, self.command_8ball)
        self.potbot.add_command("m8bs", 50, self.command_8ball)
        self.response_preface = "/me looks into his Magic 8 Ball: "
        try:
            self.responses = open(RESPONSES_PATH).readlines()
        except:
            self.initialization_failure()
            raise
        else:
            self.initialization_success()

    def command_8ball(self, caller, args):
        if len(args) == 0:
            self.potbot.send_chat_message("Ask a question!")
            return
        response_index = random.randrange(len(self.responses))
        response = self.responses[response_index][:-1]
        self.send_chat_message("{}{}".format(self.response_preface, response))
