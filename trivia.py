from time import time
from datetime import datetime
import random
from twisted.internet import reactor
from twisted.enterprise import adbapi
from twisted.internet.error import AlreadyCalled as TwistedAlreadyCalled
from twisted.internet.error import AlreadyCancelled as TwistedAlreadyCancelled
from bnutil import *
from potbotextra import PotbotService, PotbotCommand
from triviaconfig import TriviaConfig

class Question(object):
    def __init__(self, question_id, question, answer, point_multiplier, instance_id=None, ask_time=None):
        self.id = question_id
        self.question = question
        self.answer = answer
        self.point_multiplier = point_multiplier
        self.instane_id = instance_id
        self.point_multiplier = point_multiplier
        self.ask_time = ask_time
        self.answer_time = None
        self.answer_display_map = {}
        for i in range(len(self.answer)):
            if self.answer[i] == " ":
                self.answer_display_map[i] = True
            else:
                self.answer_display_map[i] = False

class Streak(object):
    def __init__(self, streak_id, length, username):
        self.id = streak_id
        self.length = length
        self.username = username

class CommandNames:
    start = "tstart"
    score = "score"
    high_scores = "hscores"
    most_answered = "manswered"
    fastest_answers = "fanswers"
    longest_streaks = "lstreak"
    hint_interval = "hinterval"
    question_interval = "qinterval"
    num_hints = "nhints"
    hint_character = "hchar"
    idle_stats = "idlestats"
    global_emote = "gemote"
    change_gender = "cgender"
    stop = "tstop"

class TriviaService(PotbotService):
    def __init__(self, potbot):
        super(self.__class__, self).__init__(potbot)
        self.config = TriviaConfig("./trivia.ini")
        self.current_question = None
        self.num_hints = self.config.num_hints
        self.hints_remaining = self.num_hints
        self.hint_interval = self.config.hint_interval
        self.hint_character = self.config.hint_character
        self.hint_probability = self.config.hint_probability
        self.question_interval = self.config.question_interval
        self.streak_threshold = self.config.streak_threshold
        if self.streak_threshold < 1:
            self.streak_threshold = float('inf')
        self.idle_stats_enabled = self.config.idle_stats
        self.idle_stats_interval = self.config.idle_stats_interval
        self.hint_timer = None
        self.question_timer = None
        self.emote = self.config.global_emote
        self.service_name = "trivia"
        self.commands = []
        self.init_commands()
        self.trigger = "trivia"
        self.current_streak = None
        self.old_streak = None

        self.on = False
        self.initializing = False
        self.initialized = False
        self.awaiting_answer = False
        self.got_correct_answer = False
        self.awaiting_hscores = False
        self.awaiting_fanswers = False
        self.awaiting_lstreaks = False
        self.idle_stats_timer = reactor.callLater(self.idle_stats_interval, self.idle_stats_timer_expired)
        self.connect_db()

    def init_commands(self):
        self.potbot.add_command(CommandNames.start, 60, self.command_start)
        self.potbot.add_command(CommandNames.score, 60, self.command_score)
        self.potbot.add_command(CommandNames.most_answered, 60, self.command_most_answered)
        self.potbot.add_command(CommandNames.high_scores, 60, self.command_high_scores)
        self.potbot.add_command(CommandNames.fastest_answers, 60, self.command_fastest_answers)
        self.potbot.add_command(CommandNames.longest_streaks, 60, self.command_longest_streaks)
        self.potbot.add_command(CommandNames.global_emote, 60, self.command_global_emote)
        self.potbot.add_command(CommandNames.hint_interval, 60, self.command_hint_interval)
        self.potbot.add_command(CommandNames.question_interval, 60, self.command_question_interval)
        self.potbot.add_command(CommandNames.idle_stats, 60, self.command_idle_stats)
        self.potbot.add_command(CommandNames.change_gender, 100, self.command_change_gender)
        self.potbot.add_command(CommandNames.stop, 60, self.command_stop)

    def connect_db(self):
        try:
            self.dbpool = adbapi.ConnectionPool("psycopg2", "dbname=potbot_trivia")
            #self.dbpool = adbapi.ConnectionPool("psycopg2", "host=52.34.72.189 dbname=potbot_trivia user=ubuntu")
            self.initialization_success()
        except:
            self.initializing = False
            self.initialization_failure()
            raise

    def start(self):
        if self.dbpool is None:
            try:
                self.connect_db()
            except Exception as e:
                print e
                self.sendchat_message("Error starting trivia: unable to connect to db")
                return
        self.initializing = True
        self.get_num_questions(self.begin_question)

    def get_num_questions(self, begin_question_callback):
        if self.initializing:
            num_questions_entries_deferred = self.dbpool.runQuery("select count(*) from question")
            num_questions_entries_deferred.addCallback(self.received_num_questions_entries)
        
    def received_num_questions_entries(self, num_questions_entries):
        if self.initializing:
            if len(num_questions_entries) < 1:
                return
            num_questions = num_questions_entries[0][0]
            self.num_questions = num_questions
            self.get_current_streak()

    def get_current_streak(self):
        if self.initializing:
            streak_entries_deferred = self.dbpool.runQuery("select streak.id,streak.length,player.username from streak,player where streak.player_id = player.id order by streak.id desc limit 1")
            streak_entries_deferred.addCallback(self.received_streak_entries)

    def received_streak_entries(self, streak_entries):
        if self.initializing:
            self.initializing = False
            self.initialized = True
            if len(streak_entries) < 1:
                self.begin_question()
                return
            streak_id, length, username = streak_entries[0]
            self.current_streak = Streak(streak_id, length, username)
            self.begin_question()

    def begin_question(self):
        if self.initialized:
            question_entries_deferred = self.dbpool.runQuery("select id,question,answer,point_multiplier from question offset floor(random()*%d) limit 1" % self.num_questions)
            question_entries_deferred.addCallback(self.received_question_entries)
            self.asking_question = True

    def received_question_entries(self, question_entries):
        if self.asking_question:
            if len(question_entries) < 1:
                print "trivia error fetching question"
                return
            question_id, question, answer, point_multiplier = question_entries[0]
            trivia_question = Question(question_id, question, answer, point_multiplier)
            self.ask_question(trivia_question)

    def ask_question(self, question):
        if self.asking_question:
            question_instance_entries_deferred = self.dbpool.runQuery("insert into question_instance (question_id,ask_time) values (%s,%s) returning id,question_id,ask_time", (question.id,self.get_timestamp()))
            question_instance_entries_deferred.addCallback(self.inserted_question_instance_entries, question)

    def inserted_question_instance_entries(self, question_instance_entries, question):
        if self.asking_question:
            if len(question_instance_entries) < 1:
                return
            question_instance_id, question_id, ask_time = question_instance_entries[0]
            self.send_chat_message("[%d] %s" % (question.id, question.question))
            self.current_question = question
            self.current_question.instance_id = question_instance_id
            self.current_question.ask_time = ask_time
            self.hints_remaining = self.num_hints
            self.asking_question = False
            self.hint_delay()

    def hint_delay(self):
        callback = self.give_hint if self.hints_remaining > 0 else self.time_expired
        self.hint_timer = reactor.callLater(self.hint_interval, callback) 
        self.hints_remaining -= 1

    def give_hint(self):
        if self.current_question is None or not self.on:
            return
        originally_hidden = []
        newly_revealed = []
        for i in self.current_question.answer_display_map:
            if not self.current_question.answer_display_map[i]: 
                originally_hidden.append(i)
                if random.random() < self.hint_probability:
                    self.current_question.answer_display_map[i] = True
                    newly_revealed.append(i)
        answer_map = self.current_question.answer_display_map
        if answer_map.values().count(False) == len(originally_hidden):
            reveal_index = random.choice(originally_hidden)
            self.current_question.answer_display_map[reveal_index] = True
            newly_revealed.append(reveal_index)
        if answer_map.values().count(True) == len(answer_map):
            unreveal_index = random.choice(newly_revealed)
            self.current_question.answer_display_map[unreveal_index] = False
        self.send_hint_message()
        self.hint_delay()

    def send_hint_message(self):
        message = "Hint: "
        for i in self.current_question.answer_display_map:
            if self.current_question.answer_display_map[i]:
                message += self.current_question.answer[i]
            else:
                message += self.hint_character
        self.send_chat_message(message)

    def chat_message_received(self, message):
        if not self.on:
            return
        username = strip_username(message.user.username)
        message_text = message.text
        if not self.got_correct_answer:
            if self.current_question is not None:
                if self.is_correct_answer(message_text):
                    self.got_correct_answer = True
                    self.cancel_timer(self.hint_timer)
                    self.current_question.answer_time = self.get_timestamp()
                    user_entries_deferred = self.dbpool.runQuery("select id,username from player where username ilike %s", (username,))
                    user_entries_deferred.addCallback(self.received_user_entries, username)

    def is_correct_answer(self, answer):
        if self.current_question is None:
            return False
        return answer.lower() == self.current_question.answer.lower()
 
    def received_user_entries(self, user_entries, username):
        if len(user_entries) < 1:
            print "user %s did not exist" % username
            if self.got_correct_answer:
                self.add_user(username)
                return
        if username.lower() != user_entries[0][1].lower():
            print "Error: username mismatch"
            return
        user_id,username = user_entries[0]
        if self.got_correct_answer:
            if self.user_has_streak(username):
                self.increment_streak(user_id)
            else:
                self.create_streak(user_id, username)
    
    def user_has_streak(self, username):
        if self.current_streak is None:
            return False
        return self.current_streak.username.lower() == username.lower()
   
    def add_user(self, username):
        username = strip_username(username)
        if self.got_correct_answer:
            player_entries_deferred = self.dbpool.runQuery("insert into player (username) values (%s) returning id,username", (username,))
            player_entries_deferred.addCallback(self.inserted_user)

    def inserted_user(self, user_entries):
        if len(user_entries) < 1:
            return
        user_id,username = user_entries[0]
        if self.got_correct_answer:
            self.create_streak(user_id, username)

    def create_streak(self, user_id, username):
        self.old_streak = self.current_streak
        self.current_streak = None
        streak_entries_deferred = self.dbpool.runQuery("insert into streak (player_id,length) values (%s,%s) returning id,player_id,length", (user_id,1))
        streak_entries_deferred.addCallback(self.inserted_streak, user_id, username)

    def inserted_streak(self, streak_entries, user_id, username):
        if len(streak_entries) < 1:
            return
        streak_id,player_id,length = streak_entries[0]
        if self.got_correct_answer:
            self.current_streak = Streak(streak_id, length, username)
            self.award_correct_answer(user_id, streak_id)

    def increment_streak(self, user_id):
        if self.current_streak is None:
            return
        streak_id = self.current_streak.id
        streak_length = self.current_streak.length + 1
        self.current_streak.length += 1
        update_deferred = self.dbpool.runQuery("update streak set length=%s where id=%s returning id,length", (streak_length,streak_id))
        update_deferred.addCallback(self.incremented_streak, user_id)

    def incremented_streak(self, streak_entry, user_id):
        if self.got_correct_answer:
            if len(streak_entry) < 1:
                return
            streak_id,length = streak_entry[0]
            self.award_correct_answer(user_id, streak_id)

    def award_correct_answer(self, user_id, streak_id):
        points_earned = self.apply_streak_bonus(1.00*float(self.current_question.point_multiplier))
        correct_answer_instance_entries_deferred = self.dbpool.runQuery("insert into correct_answer_instance (question_instance_id,player_id,streak_id,answer_time,points_earned) values (%s,%s,%s,%s,%s) returning question_instance_id,player_id,streak_id,answer_time,points_earned", (self.current_question.instance_id,user_id,streak_id,self.get_timestamp(),points_earned))
        correct_answer_instance_entries_deferred.addCallback(self.inserted_correct_answer_instance_entries, user_id)

    def apply_streak_bonus(self, base_score):
        if self.current_streak.length >= self.streak_threshold:
            return base_score + 1
        return base_score

    def inserted_correct_answer_instance_entries(self, correct_answer_instance_entries, user_id):
        if self.got_correct_answer:
            if len(correct_answer_instance_entries) != 1:
                return
            answer_info = correct_answer_instance_entries[0]
            points_earned = answer_info[4]
            user_score_entries_deferred = self.dbpool.runQuery("select username,sum(points_earned) from correct_answer_instance, player where player.id=correct_answer_instance.player_id and player.id=%s group by username", (user_id,))
            user_score_entries_deferred.addCallback(self.received_user_score_entries, points_earned)

    def received_user_score_entries(self, user_score_entries, points_earned):
        if self.got_correct_answer:
            if len(user_score_entries) < 1:
                return
            username,score = user_score_entries[0]
            rank_entries_deferred = self.dbpool.runQuery("select username,rank from (select username,row_number() over(order by sum(points_earned) desc) as rank from player,correct_answer_instance where player.id=player_id group by username) as user_scores where username ilike %s", (username,));
            rank_entries_deferred.addCallback(self.received_rank_entries, points_earned, score)

    def received_rank_entries(self, rank_entries, points_earned, score):
        if self.got_correct_answer:
            if len(rank_entries) < 1:
                return
            username,rank = rank_entries[0]
            self.send_correct_answer_message(username, points_earned, score, rank)

    def send_correct_answer_message(self, username, points_earned, score, rank, streak_broken=False):
        answer = self.current_question.answer
        question_time_delta = self.current_question.answer_time - self.current_question.ask_time
        question_seconds = question_time_delta.seconds + question_time_delta.microseconds/1000000.0
        msg = "%s is correct! %s received $%.2f. Now has %.2f. Rank: %s. Streak: %d. (%.2f sec)" % (answer, username, points_earned, score, rank, self.current_streak.length, question_seconds)
        if self.current_streak.length >= self.streak_threshold:
            msg += " ayo congrats bruh u got streak bonus 2"
        self.send_chat_message(msg)
        if self.current_streak is not None and self.old_streak is not None:
            if self.current_streak.length == 1 and self.old_streak.length >= self.streak_threshold:
                self.send_chat_message("you just poonscyped %s's streak of %d!!!" % (self.old_streak.username, self.old_streak.length))
        self.got_correct_answer = False
        self.current_question = None
        self.next_question()
  
    def time_expired(self):
        if not self.on:
            return
        self.send_chat_message(
                #"Time's up!  The correct answer was %s." % self.current_question.answer)
                "The answer(s): %s" % self.current_question.answer)
        self.next_question()

    def next_question(self):
        self.current_question = None
        self.question_timer = reactor.callLater(
                self.question_interval,
                self.begin_question)

    def idle_stats_timer_expired(self):
        if self.idle_stats_enabled:
            stats_type = random.randint(0, 3)
            me = self.potbot.config.username
            if stats_type == 0:
                self.command_high_scores(me, [])
            elif stats_type == 1:
                self.command_fastest_answers(me, [])
            elif stats_type == 2:
                self.command_longest_streaks(me, [])
            elif stats_type == 3:
                self.command_most_answered(me, [])
        self.idle_stats_timer = reactor.callLater(self.idle_stats_interval, self.idle_stats_timer_expired)

    def send_chat_message(self, message):
        if self.emote:
            message = "/me " + message
        self.potbot.send_chat_message(message)

    def command_start(self, caller, args):
        self.send_chat_message("Starting trivia...")
        self.on = True
        reactor.callLater(1, self.start)

    def command_score(self, caller, args):
        username = strip_username(caller if len(args) < 1 else args[0])
        scores_deferred = self.dbpool.runQuery("select gender,sum(points_earned) as points,count(*) as answered,extract(epoch from min(answer_time-ask_time)) as fastest,max(length) as streak,to_char(max(answer_time), 'MM/DD/YYYY HH:MI:SS AM') as last from question_instance as qi,correct_answer_instance as cai,streak as s,player as p where p.id = cai.player_id and p.id = s.player_id and cai.question_instance_id = qi.id and s.id=cai.streak_id and p.username ilike %s group by gender", (username,))
        scores_deferred.addCallback(self.cmdscore_received_scores, username)

    def cmdscore_received_scores(self, scores, username):
        if len(scores) < 1:
            self.send_chat_message("User not found: %s" % username)
            return
        gender,points,answered,fastest,streak,last = scores[0]
        try:
            fastest = int(float(fastest)*1000)
        except ValueError as e:
            #TODO: log this error
            return
        output = "%s has %s, he has answered %s questions, his fastest answer was %dms, and his longest streak was %s - he last played on %s" % (username, points, answered, fastest, streak,last)
        if gender.lower() == "female":
            output = output.replace(" he ", " she ")
            output = output.replace(" his ", " her ")
        self.send_chat_message(output)

    def command_high_scores(self, caller, args):
        hscores_deferred = self.dbpool.runQuery("select username,sum(points_earned) as score from player,correct_answer_instance where player.id=player_id group by username order by score desc limit 8")
        hscores_deferred.addCallback(self.received_hscores)
        self.awaiting_hscores = True

    def received_hscores(self, hscores):
        if self.awaiting_hscores:
            self.awaiting_hscores = False
            msg = "PT: TOP 8 Scores: "
            for i in range(len(hscores)-1):
                user,score = hscores[i]
                msg += "%s: $%.2f, " % (user, score)
            if len(hscores) > 0:
                user,score = hscores[-1]
                msg += "%s: $%.2f" % (user, score)
            self.send_chat_message(msg)

    def command_most_answered(self, caller, args):
        manswered_deferred = self.dbpool.runQuery("select username,count(*) as answered from correct_answer_instance as cai, player as p where p.id = cai.player_id group by username order by answered desc limit 8")
        manswered_deferred.addCallback(self.received_manswered)
        #TODO: remove statuses from other commands

    def received_manswered(self, manswered):
        msg = "PT: Top 8 Most Answered: "
        for i in range(len(manswered)-1):
            user,answered = manswered[i]
            msg += "%s: %s, " % (user, answered)
        if len(manswered) > 0:
            user,answered = manswered[-1]
            msg += "%s: %s" % (user, answered)
        self.send_chat_message(msg)

    def command_fastest_answers(self, caller, args):
        fanswers_deferred = self.dbpool.runQuery("select username,min(cai.answer_time - qi.ask_time) as duration from question_instance as qi, correct_answer_instance as cai, player where cai.question_instance_id = qi.id and cai.player_id=player.id group by username order by duration limit 8")
        fanswers_deferred.addCallback(self.received_fanswers)
        self.awaiting_fanswers = True

    def received_fanswers(self, fanswers):
        if self.awaiting_fanswers:
            self.awaiting_fanswers = False
            msg = "PT: Top 8 Answer Times: "
            for i in range(len(fanswers)-1):
                user,duration = fanswers[i]
                duration_int = duration.seconds*1000 + duration.microseconds/1000
                msg += "%s: %dms, " % (user, duration_int)
            if len(fanswers) > 0:
                user,duration = fanswers[-1]
                duration_int = duration.seconds*1000 + duration.microseconds/1000
                msg += "%s: %dms" % (user, duration_int)
            self.send_chat_message(msg)

    def command_longest_streaks(self, caller, args):
        lstreaks_deferred = self.dbpool.runQuery("select username,max(length) as maxstreak from player,streak where streak.player_id = player.id group by username order by maxstreak desc limit 8")
        lstreaks_deferred.addCallback(self.received_lstreaks)
        self.awaiting_lstreaks = True

    def received_lstreaks(self, lstreaks):
        if self.awaiting_lstreaks:
            self.awaiting_lstreaks = False
            msg = "PT: Top 8 Streaks: "
            for i in range(len(lstreaks)-1):
                user,streak = lstreaks[i]
                msg += "%s: %d, " % (user, streak)
            if len(lstreaks) > 0:
                user,streak = lstreaks[-1]
                msg += "%s: %d" % (user, streak)
            self.send_chat_message(msg)

    def command_question_interval(self, caller, args):
        if len(args) < 1:
            interval = self.question_interval
            self.send_chat_message(
                    "Current question interval is %d seconds" % interval)
            return
        interval_string = args[0]
        if not interval_string.isdigit():
            return
        interval = int(interval_string)
        if interval < 5:
            self.send_chat_message(
                "Question intervals must be at least 5 seconds")
            return
        self.config.set_value("question_interval", interval_string)
        self.question_interval = interval
        self.send_chat_message("Set question interval to %d seconds" % interval)

    def command_hint_interval(self, caller, args):
        if len(args) < 1:
            interval = self.hint_interval
            self.send_chat_message(
                    "Current hint interval is %d seconds" % interval)
            return
        interval_string = args[0]
        if not interval_string.isdigit():
            return
        interval = int(interval_string)
        if interval < 5:
            self.send_chat_message(
                "Hint intervals must be at least 5 seconds")
            return
        self.config.set_value("hint_interval", interval_string)
        self.hint_interval = interval
        self.send_chat_message("Set hint interval to %d seconds" % interval)

    def command_idle_stats(self, caller, args):
        if len(args) < 1:
            return
        choice = args[0].lower()
        if choice not in ["on", "off"]:
            self.send_chat_message("Valid values are \"on\", \"off\"")
            return
        if choice == "on":
            self.idle_stats_enabled = True
        elif choice == "off":
            self.idle_stats_enabled = False
        self.config.set_value("idle_stats", self.idle_stats_enabled)
        self.send_chat_message("Set idle stats to: %s" % choice.upper())
    
    def command_global_emote(self, caller, args):
        if len(args) < 1:
            return
        emote_choice = args[0].lower()
        if emote_choice not in ["on", "off"]:
            return
        if emote_choice == "on":
            self.emote = True
        elif emote_choice == "off":
            self.emote = False
        self.config.set_value("global_emote", self.emote)
        self.send_chat_message("Set global emoting to: %s" % emote_choice.upper())

    def command_change_gender(self, caller, args):
        if len(args) < 2:
            return
        username = args[0]
        gender = args[1].lower()
        male = ["male", "m", "boy"]
        female = ["female", "f", "girl"]
        if gender not in male and gender not in female:
            return
        gender = "male" if gender in male else "female"
        update_deferred = self.dbpool.runQuery("update player set gender=%s where username ilike %s returning username,gender", (gender,username))
        update_deferred.addCallback(self.cmdcgender_updated_gender, username)

    def cmdcgender_updated_gender(self, user_entries, username):
        if len(user_entries) < 1:
            self.send_chat_message("User not found: %s" % username)
            return
        username,gender = user_entries[0]
        self.send_chat_message("%s's gender set to %s" % (username, gender))

    def command_stop(self, caller, args):
        if not self.on:
            return
        self.send_chat_message("Stopping trivia...")
        self.on = False

    def cancel_timer(self, timer):
        try:
            timer.cancel()
        except (TwistedAlreadyCancelled, TwistedAlreadyCalled) as e:
            pass

    def get_timestamp(self):
        return datetime.fromtimestamp(time())

